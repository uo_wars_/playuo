﻿namespace Injection
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Sockets;
    using System.Threading;
    using System.Reflection;
    using System.Windows.Forms;
    using PlayUO;
    using PlayUO.Profiles;
    using Injection.Packets;
    using Stopwatch = System.Diagnostics.Stopwatch;
    using Cursor = PlayUO.Cursor;
    using Timer = PlayUO.Timer;
    using System.Runtime.InteropServices;


    public sealed class Seed : Engine
    {
        private static PacketHandlers Initializer { get; set; }

        public const uint Value = 3232235779;

        internal static string Username { get; private set; }
        internal static string Password { get; private set; }

        static Seed() {
            Initializer = new PacketHandlers();
        }

        [DllImport("user32.dll")]
        public static extern bool ShowWindow(IntPtr hwnd, int nCmdShow);

        private static void Print( string message ) {
            Print(message, false);
        }
        private static void Print( string message, bool readKey ) {
            Console.WriteLine("* {0}", message);
            if ( readKey )
                Console.ReadKey();
        }

        [STAThread]
        new public static void Main( string[] args ) {
            var loginAddress = Dns.GetHostAddresses( "login.kalvasflam.co" )[ 0 ].ToString();
            switch ( args.Length ) {
                case 0:
                    MessageBox.Show("Username not set.");
                    return;
                case 1:
                    MessageBox.Show("Password not set.");
                    return;
            }
            args = new string[] { "-ticket", args[ 0 ], args[ 1 ] };
            if ( Verify( args ) )
            {
                Parse(args);
                Initialize();
            }
        }

        [STAThread]
        private static void Initialize() {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler( Seed.CurrentDomain_UnhandledException );
            AppDomain.CurrentDomain.DomainUnload += ( o, ea ) => Music.Destroy();
            try {
                m_FileManager = new FileManager();
                if ( m_FileManager.Error ) {
                    m_FileManager = null;
                    GC.Collect();
                    throw new InvalidOperationException( "Unable to initialize file manager." );
                }
                MainA();
            } catch ( Exception exception ) {
                HandleException( exception );
                throw;
            }
        }

        private static bool Verify( string[] args ) {
            if ( ( args == null ) || ( args.Length != 3 ) ) {
                throw new ArgumentException( "Invalid Arguments" );
            }
            return true;
        }

        private static void Parse( string[] args ) {
            for ( int Index = 0; Index < args.Length; ) {
                var item = args[ Index = Index + 1 ];
                if ( string.Equals( item, "-ticket", StringComparison.OrdinalIgnoreCase ) )
                    throw new InvalidOperationException( "Authentication ticket already specified." );
                if ( ( Index + 2 ) > args.Length )
                    throw new InvalidOperationException( "Malformed authentication ticket specified." );
                _ticket = new AuthenticationTicket()
                {
                    _ipAddress = new IPAddress( Dns.GetHostEntry( "login.kalvasflam.co" ).AddressList[ 0 ].GetAddressBytes() ),
                    _port = 2593
                };
                //_ticket._ipAddress = IPAddress.Parse( args[ Index++ ] );
                //_ticket._port = int.Parse( args[ Index++ ] );
                Seed.Username = args[ Index++ ]; // _ticket._key = ulong.Parse( args[ num2++ ] );
                Seed.Password = args[ Index++ ]; // _ticket._contentArchive = args[ num2++ ];+
            }
        }

        
        private static void MainA() {
            if ( _ticket != null ) {
                uint num5 = 0;
                WantDirectory( "data/" );
                WantDirectory( "data/ultima/" );
                WantDirectory( "data/ultima/logs/" );
                Debug.Trace( "Entered Main()" );
                Debug.Block( "Environment" );
                Debug.Trace( "Operating System = '{0}'", Environment.OSVersion );
                Debug.Trace( ".NET Framework   = '{0}'", Environment.Version );
                Debug.Trace( "Base Directory   = '{0}'", m_FileManager.BasePath( "" ) );
                Debug.Trace( "Data Directory   = '{0}'", m_FileManager.ResolveMUL( "" ) );
                Debug.EndBlock();
                m_Timers = new ArrayList();
                m_Journal = new ArrayList();
                m_Pings = new Queue();
                m_LoadQueue = new Queue();
                m_MapLoadQueue = new Queue();
                MacroHandlers.Setup();
                Debug.Block( "Main()" );
                m_ClickTimer = new Timer( new OnTick( Seed.ClickTimer_OnTick ), SystemInformation.DoubleClickTime );
                Debug.Try( "Initializing Display" );
                m_Display = new Display();
                Preferences.Current.Layout.Apply( false );
                m_Display.KeyPreview = true;
                
                Preferences.Current.Layout.Apply( false );
                Preferences.Current.Layout.Update();
                Debug.EndTry();
                Application.DoEvents();
                Debug.Block( "Initializing DirectX" );
                InitDX();
                Debug.EndBlock();
                m_Loading = true;
                m_Ingame = false;
                Cursor.Hourglass = true;
                DrawNow();
                Debug.TimeBlock( "Initializing Animations" );
                m_Animations = new Animations();
                Debug.EndBlock();
                m_Font = new Font[ 10 ];
                m_UniFont = new UnicodeFont[ 3 ];
                Debug.TimeBlock( "Initializing Gumps" );
                m_Gumps = new Gumps();
                Debug.EndBlock();
                m_DefaultFont = GetUniFont( 3 );
                m_DefaultHue = Hues.Load( 0x3b2 );
                Renderer.SetText( "" );
                Macros.Reset();
                LoadParticles();
                Renderer.FilterEnable = false;
                Renderer.SetTexture( m_Rain );
                try {
                    m_Device.ValidateDevice( 1 );
                } catch ( Exception exception ) {
                    m_Rain.Dispose();
                    m_Rain = Texture.Empty;
                    m_SkillUp.Dispose();
                    m_SkillUp = Hues.Default.GetGump( 0x983 );
                    m_SkillDown.Dispose();
                    m_SkillDown = Hues.Default.GetGump( 0x985 );
                    m_SkillLocked.Dispose();
                    m_SkillLocked = Hues.Default.GetGump( 0x82c );
                    m_Slider.Dispose();
                    m_Slider = Hues.Default.GetGump( 0x845 );
                    for ( int i = 0; i < m_Snow.Length; i++ ) {
                        m_Snow[ i ].Dispose();
                        m_Snow[ i ] = Texture.Empty;
                    }
                    for ( int j = 0; j < m_Edge.Length; j++ ) {
                        m_Edge[ j ].Dispose();
                        m_Edge[ j ] = Texture.Empty;
                    }
                    Debug.Trace( "ValidateDevice() failed on 32-bit textures" );
                    Debug.Error( exception );
                }
                Renderer.SetTexture( null );
                m_Effects = new Effects();
                m_Loading = false;
                Point point = m_Display.PointToClient( System.Windows.Forms.Cursor.Position );
                m_EventOk = true;
                MouseMove( m_Display, new MouseEventArgs( Control.MouseButtons, 0, point.X, point.Y, 0 ) );
                Compression.CheckCache();
                Setup();
                MouseMoveQueue();
                m_EventOk = false;
                Preferences.Current.Layout.Update();
                DrawNow();
                m_MoveDelay = new TimeDelay( 0f );
                m_LastOverCheck = new TimeDelay( 0.1f );
                m_NewFrame = new TimeDelay( 0.05f );
                m_SleepMode = new TimeDelay( 7.5f );
                m_EventOk = true;
                bool flag = false;
                Animations.StartLoading();
                Unlock();
                DateTime now = DateTime.Now;
                int ticks = Ticks;
                bool flag2 = true;
                if ( Animations.IsLoading ) {
                    do {
                        DrawNow();
                    }
                    while ( !Animations.WaitLoading() );
                }
                /*Hash.Start( out num5 );
                foreach ( Assembly assembly in AppDomain.CurrentDomain.GetAssemblies() )
                {
                    Hash.Append( assembly.FullName.ToString().GetHashCode(), ref num5 );
                }
                Hash.Finish( ref num5 );*/
                Debug.Trace( "Connecting to {0}:{1}", _ticket._ipAddress, _ticket._port );
                if ( !Network.Connect( new LoginCrypto( num5 ), new IPEndPoint( _ticket._ipAddress, _ticket._port ) ) ) {
                    throw new InvalidOperationException( "Unable to connect." );
                }
                m_ServerName = _ticket._ipAddress.ToString();
                Network.Send( new PLoginSeedEx( Seed.Value ) );
                Network.Send( new PPE_Login( Seed.Username, Seed.Password ) );
                PingRequest( false );
                Compression.CheckCache();
                Stopwatch stopwatch = new Stopwatch();
                
                while ( !exiting ) {
                    stopwatch.Start();
                    m_SetTicks = false;
                    ticks = Ticks;
                    Macros.Slice();
                    ActionContext.InvokeQueue();
                    if ( Gumps.Invalidated ) {
                        if ( m_LastMouseArgs != null ) {
                            MouseMove( m_Display, m_LastMouseArgs );
                        }
                        Gumps.Invalidated = false;
                    }
                    if ( m_MouseMoved ) {
                        MouseMoveQueue();
                    }
                    if ( m_NewFrame.ElapsedReset() ) {
                        Renderer.m_Frames++;
                        m_Redraw = false;
                        RendererHook.Draw();
                        stopwatch.Reset();
                        stopwatch.Start();
                    } else if ( ( m_Redraw || m_PumpFPS ) || ( ( amMoving && Options.Current.SmoothWalk ) && IsMoving() ) ) {
                        m_Redraw = false;
                        RendererHook.Draw();
                        stopwatch.Reset();
                        stopwatch.Start();
                    }
                    if ( ( flag2 && m_Ingame ) && ( ( Party.State == PartyState.Joined ) && ( DateTime.Now >= now ) ) ) {
                        now = DateTime.Now + TimeSpan.FromSeconds( 0.5 );
                        Network.Send( new PPE_QueryPartyLocs() );
                        //Network.Send( new PPE_QueryPartyLocsEx() );
                    }
                    Thread.Sleep( ( ( World.Player == null ) || !World.Player.IsMoving ) ? 1 : 0 );
                    DoEvents();
                    if ( m_Ingame && !World.HasIdentified ) {
                        m_Display.Show();
                        Mobile player = World.Player;
                        if ( ( player != null ) && !player.Flags[ MobileFlag.Hidden ] ) {
                            player.OnSingleClick();
                            World.HasIdentified = true;
                        }
                    }
                    if ( !Network.Slice() ) {
                        flag = true;
                        break;
                    }
                    Network.Flush();
                    if (m_Timers != null)
                        TickTimers();
                    if ( amMoving && m_Ingame ) {
                        DoWalk( movingDir, false );
                    }
                    if ( m_LoadQueue != null && m_LoadQueue.Count > 0 ) {
                        for ( int k = 0; ( m_LoadQueue.Count > 0 ) && ( k < 6 ); k++ ) {
                            ( ( ILoader )m_LoadQueue.Dequeue() ).Load();
                        }
                    }
                    if ( m_MapLoadQueue.Count > 0 ) {
                        Preload( ( Worker )m_MapLoadQueue.Dequeue() );
                    }
                }
                PlayUO.Profiles.Config.Current.Save();
                Thread.Sleep( 5 );
                if ( ( m_Display != null ) && !m_Display.IsDisposed ) {
                    m_Display.Hide();
                }
                Thread.Sleep( 5 );
                Application.DoEvents();
                Thread.Sleep( 5 );
                Application.DoEvents();
                m_Animations.Dispose();
                if ( m_ItemArt != null ) {
                    m_ItemArt.Dispose();
                }
                if ( m_LandArt != null ) {
                    m_LandArt.Dispose();
                }
                if ( m_TextureArt != null ) {
                    m_TextureArt.Dispose();
                }
                m_Gumps.Dispose();
                if ( m_Sounds != null ) {
                    m_Sounds.Dispose();
                }
                if ( m_Multis != null ) {
                    m_Multis.Dispose();
                }
                m_FileManager.Dispose();
                Cursor.Dispose();
                Music.Dispose();
                Hues.Dispose();
                GRadar.Dispose();
                if ( _imageCache != null ) {
                    _imageCache.Dispose();
                    _imageCache = null;
                }
                if ( m_Rain != null ) {
                    m_Rain.Dispose();
                    m_Rain = null;
                }
                if ( m_Slider != null ) {
                    m_Slider.Dispose();
                    m_Slider = null;
                }
                if ( m_SkillUp != null ) {
                    m_SkillUp.Dispose();
                    m_SkillUp = null;
                }
                if ( m_SkillDown != null ) {
                    m_SkillDown.Dispose();
                    m_SkillDown = null;
                }
                if ( m_SkillLocked != null ) {
                    m_SkillLocked.Dispose();
                    m_SkillLocked = null;
                }
                if ( m_Snow != null ) {
                    for ( int m = 0; m < 12; m++ ) {
                        if ( m_Snow[ m ] != null ) {
                            m_Snow[ m ].Dispose();
                            m_Snow[ m ] = null;
                        }
                    }
                    m_Snow = null;
                }
                if ( m_Edge != null ) {
                    for ( int n = 0; n < 8; n++ ) {
                        if ( m_Edge[ n ] != null ) {
                            m_Edge[ n ].Dispose();
                            m_Edge[ n ] = null;
                        }
                    }
                    m_Edge = null;
                }
                if ( m_WinScrolls != null ) {
                    for ( int num8 = 0; num8 < m_WinScrolls.Length; num8++ ) {
                        if ( m_WinScrolls[ num8 ] != null ) {
                            m_WinScrolls[ num8 ].Dispose();
                            m_WinScrolls[ num8 ] = null;
                        }
                    }
                    m_WinScrolls = null;
                }
                if ( m_FormX != null ) {
                    m_FormX.Dispose();
                    m_FormX = null;
                }
                if ( m_Font != null ) {
                    for ( int num9 = 0; num9 < 10; num9++ ) {
                        if ( m_Font[ num9 ] != null ) {
                            m_Font[ num9 ].Dispose();
                            m_Font[ num9 ] = null;
                        }
                    }
                    m_Font = null;
                }
                if ( m_UniFont != null ) {
                    int length = m_UniFont.Length;
                    for ( int num11 = 0; num11 < length; num11++ ) {
                        if ( m_UniFont[ num11 ] != null ) {
                            m_UniFont[ num11 ].Dispose();
                            m_UniFont[ num11 ] = null;
                        }
                    }
                    m_UniFont = null;
                }
                if ( m_MidiTable != null ) {
                    m_MidiTable.Dispose();
                    m_MidiTable = null;
                }
                if ( m_ContainerBoundsTable != null ) {
                    m_ContainerBoundsTable.Dispose();
                    m_ContainerBoundsTable = null;
                }
                Texture.DisposeAll();
                Debug.EndBlock();
                if ( flag ) {
                    Debug.Trace( "Network error caused termination" );
                }
                Network.Close();
                Debug.Dispose();
                Speech.Dispose();
                Map.Shutdown();
                Ultima.Data.Archives.Shutdown();
                m_LoadQueue = null;
                m_MapLoadQueue = null;
                m_DefaultFont = null;
                m_DefaultHue = null;
                m_Display = null;
                m_Encoder = null;
                m_Effects = null;
                m_Skills = null;
                m_Features = null;
                m_Animations = null;
                m_LandArt = null;
                m_TextureArt = null;
                m_ItemArt = null;
                m_Gumps = null;
                m_Sounds = null;
                m_Multis = null;
                m_FileManager = null;
                m_Display = null;
                m_Font = null;
                m_UniFont = null;
                m_Device = null;
                m_MoveDelay = null;
                m_Text = null;
                m_Font = null;
                m_UniFont = null;
                m_NewFrame = null;
                m_SleepMode = null;
                m_Timers = null;
                m_SkillsGump = null;
                m_JournalGump = null;
                m_Journal = null;
                m_FileManager = null;
                m_Encoder = null;
                m_DefaultFont = null;
                m_DefaultHue = null;
                m_Random = null;
                _movementKeys = null;
                m_Prompt = null;
                m_Pings = null;
                m_PingTimer = null;
                m_MultiList = null;
                m_AllNames = null;
                m_LastOverCheck = null;
                m_LastMouseArgs = null;
                m_LastAttacker = null;
            }
            Injection.Launcher.Shell.MainForm.Close();
        }

        new public static void DrawNow()
        {
            DoEvents();
            RendererHook.Draw();
            DoEvents();
        }
    }
}
