﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using PlayUO;
namespace Injection {
    internal sealed class Pathfinding {

        private static ArrayList m_Path = new ArrayList();
        private static ArrayList m_Steps = new ArrayList();

        public static Step[] Get( IPoint3D start, IPoint3D goal )
        {
            Step step = Step.Construct( goal );

            step.G = ( 0 );
            step.F = ( 0 + GetHeuristic( start, goal ) );

            m_Steps.Add( step );

            while ( m_Steps.Count > 0 ) {

                foreach ( var o in m_Steps ) {
                    if ( o == null )
                        continue;
                    Step n = o as Step;
                    if ( n.F < step.F )
                        step = n;
                }

                if ( step.Location == goal )
                    return Reconstruct( step );

                m_Steps.Remove( step );
                m_Path.Add( step );

                foreach ( var neighbor in step.GetArea() ) {
                    if ( m_Path.Contains( neighbor ) == false ) {
                        int score = ( step.G + GetHeuristic( step, neighbor ) );
                        if ( ( m_Steps.Contains( neighbor ) == false ) || ( score < neighbor.G ) ) {
                            neighbor.S = step;
                            neighbor.G = score;
                            neighbor.F = score + GetHeuristic( neighbor.Location, goal );
                            if ( m_Steps.Contains( neighbor ) == false )
                                m_Steps.Add( neighbor );
                        }
                    }
                }
            }
            return null;
        }

        private static Step[] Reconstruct( Step current )
        {
            Stack stack = new Stack();
            stack.Push( current );
            while ( current.S != null ) {
                current = current.S;
                stack.Push( current );
            }

            object syncRoot = stack.SyncRoot;

            object[] sourceArray;
            Step[] destinationArray;

            lock ( syncRoot ) {

                sourceArray = stack.ToArray();
                stack.Clear();
                stack = null;

                destinationArray = new Step[ sourceArray.Length ];

                Array.Copy( sourceArray, 0, destinationArray, 0, destinationArray.Length );

                sourceArray = null;
            }

            return destinationArray;
        }

        private static int GetHeuristic( Step current, Step goal )
        {
            return GetHeuristic( current.Location, goal.Location );
        }
        private static int GetHeuristic( IPoint3D start, IPoint3D goal )
        {
            return ( ( start.X - goal.X ) + ( start.Y - goal.Y ) );
        }

        public sealed class Step {

            /// <summary>
            /// The parent step.
            /// </summary>
            public Step S { get; set; }

            public IPoint3D Location { get; private set; }

            public int G { get; set; }
            public int F { get; set; }

            private Step() { }

            public static Step Construct( IPoint3D p3d )
            {
                return new Step()
                {
                    Location = p3d
                };
            }

            public Step[] GetArea()
            {
                Tile[] block = Map.Felucca.GetLandBlock( this.Location.X, this.Location.Y );
                return null;

            }
        }
    }
}
