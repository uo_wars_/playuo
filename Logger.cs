﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Injection {
    public static class Logger {

        [STAThread]
        static void Main( string[] args )
        {
        }

        static StreamWriter m_Handle;
        static Indentation m_Indent;

        static Logger()
        {
            m_Indent = new Indentation();
        }

        public static void Block()
        {
            Trace( "{0}.." );
            m_Indent.Count++;

        }
        public static void EndBlock()
        {
            Trace( "done." );
        }

        public static void Trace( string message )
        {
        }
        public static void Trace( string format, object arg0 ) { }
        public static void Trace( string format, object arg0, object arg1 ) { }
        public static void Trace( string format, object arg0, object arg1, object arg2 ) { }
        public static void Trace( string format, object[] args ) { }

        public sealed class Indentation {

            public char Character { get; internal set; }
            public int Count { get; internal set; }

            public Indentation()
            {
                this.Character = ' ';
            }

            public string this[ int count ]
            {
                get { return this[ count, this.Character ]; }
            }
            public string this[ int count, char c ]
            {
                get { return new string( c, ( count * 3 ) ); }
            }

            public override string ToString()
            {
                return this[ this.Count ];
            }
        }
    }
}
