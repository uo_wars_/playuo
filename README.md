# README #

### What is this repository for? ###

* Here is a fully working injector and replacement rendering engine for the current PlayUO

### How can I help improve the client? ###

* Get involved with the Kal Vas Flam Community
* Document and report bugs
* Provide fixes and patches for developers who are interested.
