﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using SharpDX.Direct3D;
using SharpDX.Direct3D9;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using PlayUO.Profiles;
using PlayUO.Targeting;
using Ultima.Client.Terrain;
using Ultima.Data;
using Color = SharpDX.Color;
using SharpDXException = SharpDX.SharpDXException;
namespace PlayUO {
    public sealed class RendererHook : Renderer {

        private static ScreenshotContext screenshotContext = null;

        new public static void Draw()
        {
            try {
                DrawUnsafe();
            } catch ( SharpDXException exception ) {
                int code = exception.ResultCode.Code;
                if ( code == ResultCode.DeviceLost.Code ) {
                    try {
                        PresentParameters[] presentationParametersRef = new PresentParameters[] { Engine.m_PresentParams };
                        Engine.m_Device.Reset( presentationParametersRef );
                        Engine.OnDeviceReset( null, null );
                    } catch {
                    }
                    Application.DoEvents();
                    Thread.Sleep( 10 );
                } else if ( code == ResultCode.DeviceNotReset.Code ) {
                    PresentParameters[] parametersArray2 = new PresentParameters[] { Engine.m_PresentParams };
                    Engine.m_Device.Reset( parametersArray2 );
                    Engine.OnDeviceReset( null, null );
                    GC.Collect();
                } else {
                    Thread.Sleep( 10 );
                    try {
                        PresentParameters[] parametersArray3 = new PresentParameters[] { Engine.m_PresentParams };
                        Engine.m_Device.Reset( parametersArray3 );
                        Engine.OnDeviceReset( null, null );
                    } catch {
                    }
                    Debug.Error( exception );
                }
            } catch ( Exception exception2 ) {
                Debug.Error( exception2 );
            }
        }
        new public static unsafe void DrawUnsafe()
        {
            if ( ( Engine.m_Device != null ) && Validate() ) {
                PresentParameters[] parametersArray;
                if ( !_timeRefresh && ( _profile != null ) ) {
                    _profile.Reset();
                }
                RenderLights();
                Stats.Reset();
                try {
                    Engine.m_Device.Clear( ClearFlags.ZBuffer | ClearFlags.Target, Color.Black, 1f, 0 );
                } catch {
                }
                Queue toUpdateQueue = m_ToUpdateQueue;
                if ( toUpdateQueue == null ) {
                    toUpdateQueue = m_ToUpdateQueue = new Queue();
                } else if ( toUpdateQueue.Count > 0 ) {
                    toUpdateQueue.Clear();
                }
                Engine.m_Device.BeginScene();
                if ( _profile != null ) {
                    _profile._drawTime.Start();
                }
                try {
                    int num = 0;
                    int num2 = 0;
                    int num3 = 0;
                    bool preserveHue = false;
                    bool flag2 = false;
                    m_xWorld = m_yWorld = m_zWorld = 0;
                    Mobile player = World.Player;
                    if ( player != null ) {
                        preserveHue = player.Ghost;
                        flag2 = player.Flags[ MobileFlag.Warmode ];
                        m_xWorld = num = player.X;
                        m_yWorld = num2 = player.Y;
                        m_zWorld = num3 = player.Z;
                    }
                    m_Dead = preserveHue;
                    m_xScroll = 0;
                    m_yScroll = 0;
                    ArrayList textToDrawList = m_TextToDrawList;
                    if ( textToDrawList == null ) {
                        textToDrawList = m_TextToDrawList = new ArrayList();
                    } else if ( textToDrawList.Count > 0 ) {
                        textToDrawList.Clear();
                    }
                    m_TextToDraw = textToDrawList;
                    Queue miniHealthQueue = m_MiniHealthQueue;
                    if ( miniHealthQueue == null ) {
                        miniHealthQueue = m_MiniHealthQueue = new Queue();
                    } else if ( miniHealthQueue.Count > 0 ) {
                        miniHealthQueue.Clear();
                    }
                    eOffsetX = 0;
                    eOffsetY = 0;
                    if ( Engine.m_Ingame ) {
                        if ( GDesktopBorder.Instance != null ) {
                            GDesktopBorder.Instance.DoRender();
                        }
                        SetViewport( Engine.GameX, Engine.GameY, Engine.GameWidth, Engine.GameHeight );
                        if ( _profile != null ) {
                            _profile._worldTime.Start();
                        }
                        Map.Lock();
                        MapPackage package = Map.GetMap( ( num >> 3 ) - ( blockWidth >> 1 ), ( num2 >> 3 ) - ( blockHeight >> 1 ), blockWidth, blockHeight );
                        int num4 = ( ( num >> 3 ) - ( blockWidth >> 1 ) ) << 3;
                        int num5 = ( ( num2 >> 3 ) - ( blockHeight >> 1 ) ) << 3;
                        ArrayList[,] cells = package.cells;
                        int num6 = num & 7;
                        int num7 = num2 & 7;
                        int num8 = ( ( blockWidth / 2 ) * 8 ) + num6;
                        int num9 = ( ( blockHeight / 2 ) * 8 ) + num7;
                        int num10 = 0;
                        int num11 = 0;
                        num10 = Engine.GameWidth >> 1;
                        num10 -= 0x16;
                        num10 += ( 4 - num6 ) * 0x16;
                        num10 -= ( 4 - num7 ) * 0x16;
                        num11 += ( 4 - num6 ) * 0x16;
                        num11 += ( 4 - num7 ) * 0x16;
                        num11 += num3 << 2;
                        num11 += ( ( ( ( Engine.GameHeight >> 1 ) - ( ( num8 + num9 ) * 0x16 ) ) - ( ( 4 - num7 ) * 0x16 ) ) - ( ( 4 - num6 ) * 0x16 ) ) - 0x16;
                        num10--;
                        num11--;
                        num10 += Engine.GameX;
                        num11 += Engine.GameY;
                        Mobile mobile1 = World.Player;
                        bool flag3 = false;
                        m_xScroll = m_yScroll = 0;
                        if ( ( player != null ) && ( player.Walking.Count > 0 ) ) {
                            WalkAnimation animation = ( WalkAnimation )player.Walking.Peek();
                            int xOffset = 0;
                            int yOffset = 0;
                            int fOffset = 0;
                            if ( !animation.Snapshot( ref xOffset, ref yOffset, ref fOffset ) ) {
                                if ( !animation.Advance ) {
                                    xOffset = animation.xOffset;
                                    yOffset = animation.yOffset;
                                } else {
                                    xOffset = 0;
                                    yOffset = 0;
                                }
                            }
                            num10 -= xOffset;
                            num11 -= yOffset;
                            m_xScroll = xOffset;
                            m_yScroll = yOffset;
                        }
                        bool flag4 = ( ( ( !m_Invalidate && ( m_CharX == num ) ) && ( ( m_CharY == num2 ) && ( m_CharZ == num3 ) ) ) && ( ( m_WasDead == preserveHue ) && ( m_xBaseLast == num10 ) ) ) && ( m_yBaseLast == num11 );
                        m_xBaseLast = num10;
                        m_yBaseLast = num11;
                        m_Invalidate = false;
                        m_WasDead = preserveHue;
                        m_CharX = num;
                        m_CharY = num2;
                        m_CharZ = num3;
                        bool notorietyHalos = Options.Current.NotorietyHalos;
                        ArrayList list2 = new ArrayList();
                        int z = 0x7fffffff;
                        int num16 = 0x7fffffff;
                        int count = cells[ num8 + 1, num9 + 1 ].Count;
                        for ( int i = 0; i < count; i++ ) {
                            ICell cell = ( ICell )cells[ num8 + 1, num9 + 1 ][ i ];
                            Type cellType = cell.CellType;
                            if ( ( cellType == tStaticItem ) || ( cellType == tDynamicItem ) ) {
                                ITile tile = ( ITile )cell;
                                if ( ( Map.m_ItemFlags[ tile.ID & 0x3fff ][ TileFlag.None | TileFlag.Roof ] && ( tile.Z >= ( num3 + 15 ) ) ) && ( tile.Z < z ) ) {
                                    z = tile.Z;
                                }
                            }
                        }
                        count = cells[ num8, num9 ].Count;
                        for ( int j = 0; j < count; j++ ) {
                            ICell cell2 = ( ICell )cells[ num8, num9 ][ j ];
                            Type type2 = cell2.CellType;
                            if ( ( ( type2 == tStaticItem ) || ( type2 == tDynamicItem ) ) || ( type2 == tLandTile ) ) {
                                ITile tile2 = ( ITile )cell2;
                                if ( !Map.GetTileFlags( tile2.ID )[ TileFlag.None | TileFlag.Roof ] ) {
                                    int num20 = ( type2 == tLandTile ) ? tile2.SortZ : tile2.Z;
                                    if ( num20 >= ( num3 + 15 ) ) {
                                        if ( type2 == tLandTile ) {
                                            if ( num20 < z ) {
                                                z = num20;
                                            }
                                            if ( ( num3 + 0x10 ) < num16 ) {
                                                num16 = num3 + 0x10;
                                            }
                                        } else if ( num20 < z ) {
                                            z = num20;
                                        }
                                    }
                                }
                            }
                        }
                        m_CullLand = num16 < 0x7fffffff;
                        IHue hue = preserveHue ? Hues.Grayscale : Hues.Default;
                        Queue transDrawQueue = m_TransDrawQueue;
                        if ( transDrawQueue == null ) {
                            transDrawQueue = m_TransDrawQueue = new Queue();
                        } else if ( transDrawQueue.Count > 0 ) {
                            transDrawQueue.Clear();
                        }
                        RenderSettings renderSettings = Preferences.Current.RenderSettings;
                        bool itemShadows = renderSettings.ItemShadows;
                        bool characterShadows = renderSettings.CharacterShadows;
                        bool smoothCharacters = renderSettings.SmoothCharacters;
                        bool multiPreview = Engine.m_MultiPreview;
                        int num21 = 0;
                        int num22 = 0;
                        int num23 = 0;
                        int num24 = 0;
                        int num25 = 0;
                        int num26 = 0;
                        int num27 = 0;
                        int num28 = 0;
                        int num29 = 0;
                        int num30 = 0;
                        int num31 = 0;
                        int num32 = 0;
                        int num33 = 0;
                        BaseTargetHandler active = TargetManager.Active;
                        bool flag9 = flag2 || ( active != null );
                        if ( multiPreview ) {
                            if ( Gumps.IsWorldAt( Engine.m_xMouse, Engine.m_yMouse, true ) ) {
                                int tileX = 0;
                                int tileY = 0;
                                ICell cell3 = FindTileFromXY( Engine.m_xMouse, Engine.m_yMouse, ref tileX, ref tileY, false );
                                if ( cell3 == null ) {
                                    multiPreview = false;
                                } else if ( ( cell3.CellType == tLandTile ) || ( cell3.CellType == tStaticItem ) ) {
                                    num21 = tileX - num4;
                                    num22 = tileY - num5;
                                    num23 = cell3.Z + ( ( cell3.CellType == tStaticItem ) ? cell3.Height : 0 );
                                    num24 = Engine.m_MultiList.Count;
                                    num21 -= Engine.m_xMultiOffset;
                                    num22 -= Engine.m_yMultiOffset;
                                    num23 -= Engine.m_zMultiOffset;
                                    num25 = num21 + Engine.m_MultiMinX;
                                    num26 = num22 + Engine.m_MultiMinY;
                                    num27 = num21 + Engine.m_MultiMaxX;
                                    num28 = num22 + Engine.m_MultiMaxY;
                                } else {
                                    multiPreview = false;
                                }
                            } else {
                                multiPreview = false;
                            }
                        } else if ( ( ( Control.ModifierKeys & ( Keys.Control | Keys.Shift ) ) == ( Keys.Control | Keys.Shift ) ) && ( ( ( Gumps.LastOver is GSpellIcon ) && ( ( ( GSpellIcon )Gumps.LastOver ).m_SpellID == 0x39 ) ) || ( ( Gumps.LastOver is GContainerItem ) && ( ( ( ( GContainerItem )Gumps.LastOver ).Item.ID & 0x3fff ) == 0x1f65 ) ) ) ) {
                            int num36 = 1 + ( ( int )( Engine.Skills[ SkillName.Magery ].Value / 15f ) );
                            num33 = 0xff6666;
                            num29 = ( player.X - num4 ) - num36;
                            num30 = ( player.Y - num5 ) - num36;
                            num31 = ( player.X - num4 ) + num36;
                            num32 = ( player.Y - num5 ) + num36;
                        } else if ( active is ServerTargetHandler ) {
                            ServerTargetHandler handler2 = active as ServerTargetHandler;
                            int num37 = 0;
                            int num38 = -1;
                            bool flag10 = false;
                            if ( ( handler2.Action == TargetAction.MeteorSwarm ) || ( handler2.Action == TargetAction.ChainLightning ) ) {
                                num37 = 0xff6666;
                                num38 = 2;
                            } else if ( handler2.Action == TargetAction.MassCurse ) {
                                num37 = 0xff6666;
                                num38 = 3;
                            } else if ( handler2.Action == TargetAction.MassDispel ) {
                                num37 = 0xff6666;
                                num38 = 8;
                            } else if ( handler2.Action == TargetAction.Reveal ) {
                                num37 = 0x99ccff;
                                num38 = 1 + ( ( int )( Engine.Skills[ SkillName.Magery ].Value / 20f ) );
                            } else if ( handler2.Action == TargetAction.DetectHidden ) {
                                num37 = 0x99ccff;
                                num38 = ( int )( Engine.Skills[ SkillName.DetectingHidden ].Value / 10f );
                            } else if ( handler2.Action == TargetAction.ArchProtection ) {
                                num37 = 0x99ccff;
                                num38 = Engine.ServerFeatures.AOS ? 2 : 3;
                            } else if ( handler2.Action == TargetAction.ArchCure ) {
                                num37 = 0x99ccff;
                                num38 = 3;
                            } else if ( handler2.Action == TargetAction.WallOfStone ) {
                                num37 = 0xff6666;
                                num38 = 1;
                                flag10 = true;
                            } else if ( ( ( handler2.Action == TargetAction.EnergyField ) || ( handler2.Action == TargetAction.FireField ) ) || ( ( handler2.Action == TargetAction.ParalyzeField ) || ( handler2.Action == TargetAction.PoisonField ) ) ) {
                                num37 = 0xff6666;
                                num38 = 2;
                                flag10 = true;
                            }
                            if ( ( num37 != 0 ) && Gumps.IsWorldAt( Engine.m_xMouse, Engine.m_yMouse, true ) ) {
                                int tileX = 0;
                                int tileY = 0;
                                ICell cell4 = FindTileFromXY( Engine.m_xMouse, Engine.m_yMouse, ref tileX, ref tileY, false );
                                if ( ( cell4 != null ) && ( ( ( cell4.CellType == tLandTile ) || ( cell4.CellType == tStaticItem ) ) || ( ( cell4.CellType == tMobileCell ) || ( cell4.CellType == tDynamicItem ) ) ) ) {
                                    num33 = num37;
                                    if ( num38 >= 0 ) {
                                        if ( flag10 ) {
                                            bool flag11;
                                            int x = player.X - tileX;
                                            int y = player.Y - tileY;
                                            int offsetX = x - y;
                                            int offsetY = x + y;
                                            if ( ( offsetX >= 0 ) && ( offsetY >= 0 ) ) {
                                                flag11 = false;
                                            } else if ( offsetX >= 0 ) {
                                                flag11 = true;
                                            } else if ( offsetY >= 0 ) {
                                                flag11 = true;
                                            } else {
                                                flag11 = false;
                                            }
                                            if ( flag11 ) {
                                                num29 = ( tileX - num4 ) - num38;
                                                num31 = ( tileX - num4 ) + num38;
                                                num30 = tileY - num5;
                                                num32 = tileY - num5;
                                            } else {
                                                num29 = tileX - num4;
                                                num31 = tileX - num4;
                                                num30 = ( tileY - num5 ) - num38;
                                                num32 = ( tileY - num5 ) + num38;
                                            }
                                        } else {
                                            num29 = ( tileX - num4 ) - num38;
                                            num30 = ( tileY - num5 ) - num38;
                                            num31 = ( tileX - num4 ) + num38;
                                            num32 = ( tileY - num5 ) + num38;
                                        }
                                    }
                                }
                            }
                        }
                        int item2_Index = 0;
                        int item2_Z = 0;
                        IHue grayscale = null;
                        bool flag12 = false;
                        bool flag13 = false;
                        DynamicItem item = null;
                        StaticItem item2 = null;
                        Item owner = null;
                        bool xDouble = false;
                        int size = ( cellWidth < cellHeight ) ? ( cellWidth - 1 ) : ( cellHeight - 1 );
                        TerrainMeshProvider current = TerrainMeshProviders.Current;
                        foreach ( MapSubgroup subgroup in GetMapSubgroups( size ) ) {
                            int x = subgroup.x;
                            int y = subgroup.y;
                            bool flag15 = false;
                            int num51 = ( ( x - y ) * 0x16 ) + num10;
                            int num52 = ( ( x + y ) * 0x16 ) + num11;
                            Stopwatch stopwatch = null;
                            int num53 = cells[ x, y ].Count;
                            for ( int k = 0; k < num53; k++ ) {
                                TileFlags item2_Flags;
                                bool flag17;
                                int num55;
                                Texture texture2;
                                float num67;
                                int num70;
                                int num71;
                                if ( stopwatch != null ) {
                                    stopwatch.Stop();
                                    stopwatch = null;
                                }
                                ICell cell5 = ( ICell )cells[ x, y ][ k ];
                                Type type3 = cell5.CellType;
                                if ( subgroup.ground == flag15 ) {
                                    if ( type3 == tLandTile ) {
                                        flag15 = true;
                                    }
                                } else {
                                    if ( type3 == tLandTile ) {
                                        flag15 = true;
                                    }
                                    if ( !( type3 == tStaticItem ) && !( type3 == tDynamicItem ) ) {
                                        goto Label_1305;
                                    }
                                    flag12 = type3 == tStaticItem;
                                    flag13 = !flag12;
                                    IItem item3 = ( IItem )cell5;
                                    if ( flag12 ) {
                                        item2 = ( StaticItem )item3;
                                        item2_Index = item2.m_ID;
                                        item2_Z = item2.m_Z;
                                        item2_Flags = Map.m_ItemFlags[ item2_Index ];
                                        if ( ( z < 0x7fffffff ) && ( ( item2_Z >= z ) || item2_Flags[ TileFlag.None | TileFlag.Roof ] ) ) {
                                            continue;
                                        }
                                        grayscale = item2.m_Hue;
                                        xDouble = false;
                                    } else {
                                        item = ( DynamicItem )item3;
                                        item2_Index = item.m_ID;
                                        item2_Z = item.m_Z;
                                        item2_Flags = Map.m_ItemFlags[ item2_Index ];
                                        if ( ( z < 0x7fffffff ) && ( ( item2_Z >= z ) || item2_Flags[ TileFlag.None | TileFlag.Roof ] ) ) {
                                            continue;
                                        }
                                        grayscale = item.m_Hue;
                                        owner = item.m_Item;
                                        item2_Index = Map.GetDispID( item2_Index, ( ushort )owner.Amount, ref xDouble );
                                    }
                                    if ( !item2_Flags[ TileFlag.Internal ] ) {
                                        flag17 = false;
                                        num55 = 0xffffff;
                                        if ( ( ( ( num33 != 0 ) && ( x >= num29 ) ) && ( ( y >= num30 ) && ( x <= num31 ) ) ) && ( y <= num32 ) ) {
                                            grayscale = Hues.Grayscale;
                                            num55 = num33;
                                            flag17 = true;
                                        }
                                        if ( itemShadows && ShadowManager.HasShadow( item2_Index ) ) {
                                            Texture texture = Hues.Shadow.GetItem( item2_Index );
                                            if ( ( texture != null ) && !texture.IsEmpty() ) {
                                                int num56 = ( num51 + 0x16 ) - ( texture.Width >> 1 );
                                                int num57 = ( ( num52 - ( item2_Z << 2 ) ) + 0x2b ) - texture.Height;
                                                PushAlpha( 0.5f );
                                                texture.DrawShadow( num56, num57, ( float )( texture.Width / 2 ), ( float )( texture.Height - 8 ) );
                                                PopAlpha();
                                            }
                                        }
                                        if ( ( flag4 && flag12 ) && !flag17 ) {
                                            StaticItem item5 = item2;
                                            if ( item5.m_bInit ) {
                                                if ( item5.m_bDraw ) {
                                                    PushAlpha( item5.m_fAlpha );
                                                    SetTexture( item5.m_sDraw );
                                                    //try {
                                                        fixed ( TransformedColoredTextured* texturedRef = item5.m_vPool ) {
                                                            PushQuad( texturedRef );
                                                        }
                                                    //} finally {
                                                    //    texturedRef = null;
                                                    //}
                                                    PopAlpha();
                                                }
                                                continue;
                                            }
                                        }
                                        if ( ( !flag12 && ( owner != null ) ) && ( owner.ID == 0x2006 ) ) {
                                            if ( owner.CorpseSerial == 0 ) {
                                                IHue itemHue;
                                                int amount = owner.Amount;
                                                GraphicTranslation translation = GraphicTranslators.Corpse[ amount ];
                                                if ( translation != null ) {
                                                    amount = translation.FallbackId;
                                                    grayscale = Hues.Load( translation.FallbackData ^ 0x8000 );
                                                }
                                                int animDirection = Engine.GetAnimDirection( owner.Direction );
                                                int actionID = Engine.m_Animations.ConvertAction( amount, owner.Serial, owner.X, owner.Y, animDirection, GenericAction.Die, null );
                                                int num61 = Engine.m_Animations.GetFrameCount( amount, actionID, animDirection );
                                                int xCenter = num51 + 0x17;
                                                int yCenter = ( num52 - ( item2_Z << 2 ) ) + 20;
                                                if ( preserveHue ) {
                                                    itemHue = hue;
                                                } else {
                                                    itemHue = grayscale;
                                                }
                                                int textureX = 0;
                                                int textureY = 0;
                                                Frame frame = Engine.m_Animations.GetFrame( owner, amount, actionID, animDirection, num61 - 1, xCenter, yCenter, itemHue, ref textureX, ref textureY, preserveHue );
                                                owner.DrawGame( frame.Image, textureX, textureY, num55 );
                                                owner.MessageX = ( textureX + frame.Image.xMin ) + ( ( frame.Image.xMax - frame.Image.xMin ) / 2 );
                                                owner.MessageY = textureY;
                                                owner.BottomY = textureY + frame.Image.yMax;
                                                owner.MessageFrame = m_ActFrames;
                                                List<Item> sortedCorpseItems = owner.GetSortedCorpseItems();
                                                for ( int n = 0; n < sortedCorpseItems.Count; n++ ) {
                                                    Item item6 = sortedCorpseItems[ n ];
                                                    if ( item6.Parent == owner ) {
                                                        if ( !preserveHue ) {
                                                            itemHue = Hues.GetItemHue( item6.ID, item6.Hue );
                                                        }
                                                        frame = Engine.m_Animations.GetFrame( item6, item6.AnimationId, actionID, animDirection, num61 - 1, xCenter, yCenter, itemHue, ref textureX, ref textureY, preserveHue );
                                                        item6.DrawGame( frame.Image, textureX, textureY, num55 );
                                                    }
                                                }
                                            }
                                        } else {
                                            IHue hue4;
                                            texture2 = null;
                                            num67 = 1f;
                                            int itemID = item2_Index & 0x3fff;
                                            if ( preserveHue ) {
                                                hue4 = hue;
                                            } else if ( flag12 ) {
                                                hue4 = grayscale;
                                            } else if ( ( owner != null ) && owner.Flags[ ItemFlag.Hidden ] ) {
                                                hue4 = Hues.Grayscale;
                                                num67 = 0.5f;
                                            } else {
                                                hue4 = grayscale;
                                            }
                                            AnimData anim = Map.GetAnim( itemID );
                                            int num69 = item2_Index;
                                            if ( ( anim.frameCount != 0 ) && item2_Flags[ TileFlag.Animation ] ) {
                                                num69 += anim[ ( m_Frames / ( anim.frameInterval + 1 ) ) % anim.frameCount ];
                                            }

                                            hue4 = Hues.Default;

                                            /*
                                             * 
                                             * 
                                             * 
                                             * 
                                             * 
                                             * 
                                             * This is where we change the game roofs or walls into being transparent.
                                             *
                                             * 
                                             * 
                                             * 
                                             * 
                                             * 
                                             * 
                                             * 
                                             * 
                                             * */

                                            //var testData = Map.GetAnim( itemID );
                                            //var test = ( ( IItem )null );
                                            //var testTexture = test.GetItem( Hues.Load( 1153 ), testData.


                                            texture2 = item3.GetItem( hue4, ( short )num69 );
                                            if ( ( texture2 != null ) && !texture2.IsEmpty() ) {
                                                num70 = ( num51 + 0x16 ) - ( texture2.Width >> 1 );
                                                num71 = ( ( num52 - ( item2_Z << 2 ) ) + 0x2b ) - texture2.Height;
                                                if ( flag3 && item2_Flags[ TileFlag.Foliage ] ) {
                                                    Rectangle rectangle = new Rectangle( num70 + texture2.xMin, num71 + texture2.yMin, texture2.xMax, texture2.yMax );
                                                    if ( rectangle.IntersectsWith( m_FoliageCheck ) ) {
                                                        num67 *= 0.4f;
                                                    }
                                                } else if ( item2_Flags[ TileFlag.None | TileFlag.Translucent ] ) {
                                                    num67 *= 0.9f;
                                                }
                                                if ( !flag13 ) {
                                                    goto Label_1295;
                                                }
                                                if ( owner != null ) {
                                                    PushAlpha( num67 );
                                                    owner.DrawGame( texture2, num70, num71, num55 );
                                                    if ( xDouble ) {
                                                        owner.DrawGame( texture2, num70 + 5, num71 + 5, num55 );
                                                    }
                                                    PopAlpha();
                                                    goto Label_122A;
                                                }
                                                PushAlpha( num67 );
                                                texture2.DrawGame( num70, num71, num55 );
                                                if ( xDouble ) {
                                                    texture2.DrawGame( num70 + 5, num71 + 5, num55 );
                                                }
                                                PopAlpha();
                                            }
                                        }
                                    }
                                }
                                continue;
                            Label_122A:
                                if ( m_Transparency ) {
                                    transDrawQueue.Enqueue( TransparentDraw.PoolInstance( texture2, num70, num71, num67, xDouble ) );
                                }
                                owner.MessageX = ( num70 + texture2.xMin ) + ( ( texture2.xMax - texture2.xMin ) / 2 );
                                owner.MessageY = num71;
                                owner.BottomY = num71 + texture2.yMax;
                                owner.MessageFrame = m_ActFrames;
                                continue;
                            Label_1295:
                                PushAlpha( num67 );
                                if ( item2_Flags[ TileFlag.Animation ] ) {
                                    texture2.DrawGame( num70, num71, num55, item2.m_vPool );
                                } else {
                                    item2.m_bDraw = texture2.DrawGame( num70, num71, num55, item2.m_vPool );
                                    item2.m_fAlpha = num67;
                                    item2.m_bInit = !flag17;
                                    item2.m_sDraw = texture2;
                                }
                                PopAlpha();
                                continue;
                            Label_1305:
                                if ( type3 == tLandTile ) {
                                    LandTile tile3 = ( LandTile )cell5;
                                    item2_Z = tile3.m_Z;
                                    if ( tile3.m_ID != 2 ) {
                                        int num73 = num51;
                                        int num74 = num52;
                                        if ( ( ( num73 < ( Engine.GameX + Engine.GameWidth ) ) && ( ( num73 + 0x2c ) > Engine.GameX ) ) && ( ( num16 >= 0x7fffffff ) || ( item2_Z < num16 ) ) ) {
                                            IHue hue5 = hue;
                                            int baseColor = 0xffffff;
                                            if ( ( ( ( num33 != 0 ) && ( x >= num29 ) ) && ( ( y >= num30 ) && ( x <= num31 ) ) ) && ( y <= num32 ) ) {
                                                hue5 = Hues.Grayscale;
                                                baseColor = num33;
                                            }
                                            tile3.EnsureMesh( hue5, current );
                                            if ( tile3._mesh != null ) {
                                                SetTexture( tile3.m_sDraw );
                                                tile3._mesh.Update( tile3, baseColor );
                                                tile3._mesh.Render( num73, num74 );
                                            }
                                        }
                                    }
                                    continue;
                                }
                                if ( ( type3 == tMobileCell ) && ( ( z >= 0x7fffffff ) || ( cell5.Z < z ) ) ) {
                                    IAnimatedCell cell6 = ( IAnimatedCell )cell5;
                                    int body = 0;
                                    int direction = 0;
                                    int num78 = 0;
                                    int action = 0;
                                    int num80 = 0;
                                    Mobile mobile = ( ( MobileCell )cell5 ).m_Mobile;
                                    int frames = 0;
                                    int num82 = 0;
                                    int num83 = 0;
                                    if ( mobile.Player ) {
                                        flag3 = true;
                                    }
                                    if ( mobile.Walking.Count > 0 ) {
                                        WalkAnimation animation2 = ( WalkAnimation )mobile.Walking.Peek();
                                        if ( !animation2.Snapshot( ref num82, ref num83, ref frames ) ) {
                                            if ( !animation2.Advance ) {
                                                num82 = animation2.xOffset;
                                                num83 = animation2.yOffset;
                                            } else {
                                                num82 = 0;
                                                num83 = 0;
                                            }
                                            frames = animation2.Frames;
                                            mobile.SetLocation( animation2.NewX, animation2.NewY, animation2.NewZ );
                                            ( ( WalkAnimation )mobile.Walking.Dequeue() ).Dispose();
                                            if ( mobile.Player ) {
                                                if ( Engine.amMoving ) {
                                                    Engine.DoWalk( Engine.movingDir, true );
                                                }
                                                eOffsetX += num82;
                                                eOffsetY += num83;
                                            }
                                            if ( mobile.Walking.Count == 0 ) {
                                                mobile.Direction = ( byte )animation2.NewDir;
                                                mobile.IsMoving = false;
                                                mobile.MovedTiles = 0;
                                                mobile.HorseFootsteps = 0;
                                            } else {
                                                ( ( WalkAnimation )mobile.Walking.Peek() ).Start();
                                            }
                                            toUpdateQueue.Enqueue( mobile );
                                        }
                                    }
                                    List<Item> sortedItems = mobile.GetSortedItems();
                                    cell6.GetPackage( ref body, ref action, ref direction, ref num80, ref num78 );
                                    int num84 = num80;
                                    int num85 = Engine.m_Animations.GetFrameCount( body, action, direction );
                                    if ( num85 != 0 ) {
                                        IHue notoriety;
                                        Frame frame2;
                                        num84 = num84 % num85;
                                        int num86 = num51 + 0x16;
                                        int num87 = ( num52 - ( cell6.Z << 2 ) ) + 0x16;
                                        num86++;
                                        num87 -= 2;
                                        num86 += num82;
                                        num87 += num83;
                                        if ( frames != 0 ) {
                                            num84 += frames;
                                            num84 = num84 % num85;
                                            num80 += frames;
                                            num80 = num80 % num85;
                                        }
                                        if ( ( mobile.Human && mobile.IsMoving ) && ( mobile.LastFrame != num80 ) ) {
                                            int? nullable = null;
                                            if ( mobile.IsMounted && ( ( direction & 0x80 ) != 0 ) ) {
                                                switch ( num80 ) {
                                                    case 1:
                                                        nullable = 0x129;
                                                        break;

                                                    case 3:
                                                        nullable = 0x12a;
                                                        break;
                                                }
                                            } else if ( num80 == 1 ) {
                                                nullable = 0x12b;
                                            } else if ( num80 == 6 ) {
                                                nullable = 300;
                                            }
                                            if ( nullable.HasValue && !Preferences.Current.Footsteps.Volume.IsMuted ) {
                                                float volume = 0.675f;
                                                float frequency = ( float )( ( ( Engine.Random.NextDouble() * 2.0 ) - 1.0 ) / 14.0 );
                                                if ( mobile.Player ) {
                                                    Engine.Sounds.PlaySound( nullable.Value, -1, -1, -1, volume, frequency );
                                                } else {
                                                    Engine.Sounds.PlaySound( nullable.Value, mobile.X, mobile.Y, mobile.Z, volume, frequency );
                                                }
                                            }
                                            mobile.LastFrame = num80;
                                        }
                                        bool flag19 = false;
                                        bool flag20 = false;
                                        IHue hue7 = null;
                                        bool flag21 = false;
                                        float alpha = 1f;
                                        int color = 0xffffff;
                                        if ( preserveHue ) {
                                            notoriety = hue;
                                            flag20 = true;
                                            hue7 = notoriety;
                                        } else if ( ( ( ( num33 != 0 ) && ( x >= num29 ) ) && ( ( y >= num30 ) && ( x <= num31 ) ) ) && ( y <= num32 ) ) {
                                            notoriety = Hues.Grayscale;
                                            color = num33;
                                            flag20 = true;
                                            hue7 = notoriety;
                                            flag19 = false;
                                        } else if ( ( mobile.Flags.Value & -224 ) != 0 ) {
                                            notoriety = Hues.Load( 0x8485 );
                                            flag20 = true;
                                            hue7 = notoriety;
                                            flag19 = false;
                                        } else if ( mobile.Flags[ MobileFlag.Hidden ] ) {
                                            notoriety = Hues.Grayscale;
                                            flag20 = true;
                                            hue7 = notoriety;
                                        } else if ( ( ( Engine.m_Highlight == mobile ) || ( m_AlwaysHighlight == mobile.Serial ) ) && !mobile.Player ) {
                                            notoriety = Hues.GetNotoriety( mobile.Notoriety );
                                            flag20 = true;
                                            hue7 = notoriety;
                                            flag19 = true;
                                        } else if ( mobile.IsDeadPet ) {
                                            notoriety = Hues.Grayscale;
                                            flag20 = true;
                                            hue7 = notoriety;
                                            flag19 = true;
                                        } else {
                                            notoriety = Hues.Load( num78 );
                                        }
                                        int num92 = 0;
                                        int num93 = 0;
                                        try {
                                            frame2 = Engine.m_Animations.GetFrame( mobile, body, action, direction, num84, num86, num87, notoriety, ref num92, ref num93, flag20 );
                                        } catch {
                                            frame2 = Engine.m_Animations.GetFrame( mobile, body, action, direction, num84, num86, num87, notoriety, ref num92, ref num93, flag20 );
                                        }
                                        bool flag22 = false;
                                        float num94 = 1f;
                                        int num95 = -1;
                                        int num96 = -1;
                                        Texture t = null;
                                        if ( ( frame2.Image != null ) && !frame2.Image.IsEmpty() ) {
                                            mobile.MessageFrame = m_ActFrames;
                                            mobile.ScreenX = mobile.MessageX = num86;
                                            mobile.ScreenY = num87;
                                            mobile.MessageY = num93;
                                            if ( mobile.Player ) {
                                                m_FoliageCheck = new Rectangle( num92, num93, frame2.Image.Width, frame2.Image.Height );
                                            }
                                            if ( ( ( flag2 && !mobile.Player ) && ( ( mobile.Notoriety >= Notoriety.Innocent ) && ( mobile.Notoriety <= Notoriety.Vendor ) ) ) && notorietyHalos ) {
                                                Texture playerHalo = Engine.ImageCache.PlayerHalo;
                                                playerHalo.DrawGame( num86 - ( playerHalo.Width >> 1 ), num87 - ( playerHalo.Height >> 1 ), Engine.C16232( Hues.GetNotorietyData( mobile.Notoriety ).colors[ 0x2f ] ) );
                                            }
                                            if ( characterShadows && !mobile.IsDead ) {
                                                int num97 = 0;
                                                int num98 = 0;
                                                PushAlpha( 0.5f );
                                                Frame frame3 = Engine.m_Animations.GetFrame( mobile, body, action, direction, num84, num86, num87, Hues.Shadow, ref num97, ref num98, false );
                                                if ( ( frame3.Image != null ) && !frame3.Image.IsEmpty() ) {
                                                    frame3.Image.DrawShadow( num97, num98, ( float )( num86 - num97 ), ( float )( num87 - num98 ) );
                                                }
                                                if ( mobile.HumanOrGhost ) {
                                                    bool isMounted = mobile.IsMounted;
                                                    for ( int num99 = 0; num99 < sortedItems.Count; num99++ ) {
                                                        Item item7 = sortedItems[ num99 ];
                                                        if ( ( ( item7.Layer == Layer.Mount ) || ( item7.Layer == Layer.OneHanded ) ) || ( item7.Layer == Layer.TwoHanded ) ) {
                                                            int animationId = item7.AnimationId;
                                                            int num101 = action;
                                                            int num102 = num80;
                                                            if ( item7.Layer == Layer.Mount ) {
                                                                if ( mobile.IsMoving ) {
                                                                    num101 = ( ( direction & 0x80 ) == 0 ) ? 0 : 1;
                                                                } else if ( mobile.Animation == null ) {
                                                                    num101 = 2;
                                                                } else if ( num101 == 0x17 ) {
                                                                    num101 = 0;
                                                                } else if ( num101 == 0x18 ) {
                                                                    num101 = 1;
                                                                } else {
                                                                    if ( ( num101 < 0x19 ) || ( num101 > 0x1d ) ) {
                                                                        continue;
                                                                    }
                                                                    num101 = 2;
                                                                }
                                                            } else if ( isMounted ) {
                                                                if ( mobile.IsMoving ) {
                                                                    num101 = 0x17 + ( ( direction & 0x80 ) >> 7 );
                                                                } else if ( mobile.Animation == null ) {
                                                                    num101 = 0x19;
                                                                }
                                                            }
                                                            int num103 = item7.Hue;
                                                            if ( item7.Layer == Layer.Mount ) {
                                                                int bodyID = animationId;
                                                                Engine.m_Animations.Translate( ref bodyID, ref num103 );
                                                            }
                                                            int num105 = Engine.m_Animations.GetFrameCount( animationId, num101, direction );
                                                            if ( num105 == 0 ) {
                                                                num102 = 0;
                                                            } else {
                                                                num102 = num102 % num105;
                                                            }
                                                            frame3 = Engine.m_Animations.GetFrame( item7, animationId, num101, direction, num102, num86, num87, Hues.Shadow, ref num97, ref num98, false );
                                                            if ( ( frame3.Image != null ) && !frame3.Image.IsEmpty() ) {
                                                                frame3.Image.DrawShadow( num97, num98, ( float )( num86 - num97 ), ( float )( num87 - num98 ) );
                                                            }
                                                        }
                                                    }
                                                }
                                                PopAlpha();
                                            }
                                            if ( ( mobile.Flags[ MobileFlag.Hidden ] || ( body == 970 ) ) || ( mobile.IsDeadPet || mobile.Ghost ) ) {
                                                flag21 = true;
                                                alpha = 0.5f;
                                            }
                                            PushAlpha( alpha );
                                            if ( mobile.HumanOrGhost && mobile.IsMounted ) {
                                                num94 = _alphaValue;
                                                t = frame2.Image;
                                                num95 = num92;
                                                num96 = num93;
                                                flag22 = true;
                                            } else {
                                                flag22 = false;
                                            }
                                            if ( !mobile.Ghost && !flag22 ) {
                                                mobile.DrawGame( frame2.Image, num92, num93, color );
                                            }
                                            PopAlpha();
                                        }
                                        int num106 = 0;
                                        int num107 = ( ( num52 - ( cell5.Z << 2 ) ) + 0x12 ) - ( num106 = Engine.m_Animations.GetHeight( body, action, direction ) );
                                        int num108 = num51 + 0x16;
                                        num108 += num82;
                                        num107 += num83;
                                        if ( ( Options.Current.MiniHealth && mobile.OpenedStatus ) && ( !mobile.Ghost && ( mobile.MaximumHitPoints > 0 ) ) ) {
                                            miniHealthQueue.Enqueue( MiniHealthEntry.PoolInstance( num108, ( num107 + 4 ) + num106, mobile ) );
                                        }
                                        if ( mobile.HumanOrGhost ) {
                                            bool flag24 = mobile.IsMounted;
                                            bool flag25 = false;
                                            for ( int num109 = 0; num109 < sortedItems.Count; num109++ ) {
                                                Item item8 = sortedItems[ num109 ];
                                                if ( mobile.Ghost && ( item8.Layer != Layer.OuterTorso ) ) {
                                                    continue;
                                                }
                                                int num110 = item8.AnimationId;
                                                int num111 = action;
                                                int num112 = num80;
                                                if ( mobile.Ghost ) {
                                                    num110 = 970;
                                                }
                                                if ( item8.Layer == Layer.Mount ) {
                                                    if ( mobile.IsMoving ) {
                                                        num111 = ( ( direction & 0x80 ) == 0 ) ? 0 : 1;
                                                    } else if ( mobile.Animation == null ) {
                                                        num111 = 2;
                                                    } else if ( num111 == 0x17 ) {
                                                        num111 = 0;
                                                    } else if ( num111 == 0x18 ) {
                                                        num111 = 1;
                                                    } else {
                                                        if ( ( num111 < 0x19 ) || ( num111 > 0x1d ) ) {
                                                            goto Label_1EA5;
                                                        }
                                                        num111 = 2;
                                                    }
                                                } else if ( flag24 ) {
                                                    if ( mobile.IsMoving ) {
                                                        num111 = 0x17 + ( ( direction & 0x80 ) >> 7 );
                                                    } else if ( mobile.Animation == null ) {
                                                        num111 = 0x19;
                                                    }
                                                }
                                                float num113 = alpha;
                                                int num114 = item8.Hue;
                                                if ( item8.Layer == Layer.Mount ) {
                                                    int num115 = num110;
                                                    Engine.m_Animations.Translate( ref num115, ref num114 );
                                                }
                                                if ( !flag20 || ( ( item8.Layer == Layer.Mount ) && flag19 ) ) {
                                                    notoriety = Hues.GetItemHue( item8.ID, item8.Hue );
                                                    flag25 = false;
                                                } else {
                                                    notoriety = hue7;
                                                    flag25 = flag20;
                                                }
                                                int num116 = Engine.m_Animations.GetFrameCount( num110, num111, direction );
                                                if ( num116 == 0 ) {
                                                    num112 = 0;
                                                } else {
                                                    num112 = num112 % num116;
                                                }
                                                frame2 = Engine.m_Animations.GetFrame( item8, num110, num111, direction, num112, num86, num87, notoriety, ref num92, ref num93, flag25 );
                                                if ( ( frame2.Image != null ) && !frame2.Image.IsEmpty() ) {
                                                    PushAlpha( mobile.Ghost ? 0.5f : num113 );
                                                    item8.DrawGame( frame2.Image, num92, num93, color );
                                                    PopAlpha();
                                                }
                                            Label_1EA5:
                                                if ( ( item8.Layer == Layer.Mount ) && flag22 ) {
                                                    PushAlpha( num94 );
                                                    mobile.DrawGame( t, num95, num96, color );
                                                    PopAlpha();
                                                }
                                            }
                                        }
                                        if ( flag9 ) {
                                            int num117 = -1;
                                            if ( mobile == TargetManager.LastOffensiveTarget ) {
                                                num117 = 0xff2200;
                                            } else if ( mobile == TargetManager.LastDefensiveTarget ) {
                                                num117 = 0x22ddff;
                                            } else if ( mobile == TargetManager.LastTarget ) {
                                                num117 = 0xcccccc;
                                            }
                                            if ( num117 != -1 ) {
                                                DrawPlayerIcon( num86, num87, Engine.ImageCache.LastTargetIcon, num117 );
                                            }
                                        }
                                        if ( notorietyHalos ) {
                                            if ( mobile.m_IsFriend ) {
                                                DrawPlayerIcon( num86, num93 - 10, Engine.ImageCache.PlayerAlly, 0xffffff );
                                            } else if ( ( ( !mobile.Player && player.Warmode ) && ( mobile.Visible && mobile.Human ) ) && ( !mobile.IsDead && TargetManager.IsAcquirable( player, mobile ) ) ) {
                                                DrawPlayerIcon( num86, num93 - 10, Engine.ImageCache.PlayerEnemy, 0xffffff );
                                            }
                                        }
                                    }
                                }
                            }
                            if ( stopwatch != null ) {
                                stopwatch.Stop();
                            }
                            if ( ( ( multiPreview && ( x >= num25 ) ) && ( ( x <= num27 ) && ( y >= num26 ) ) ) && ( y <= num28 ) ) {
                                int num118 = num51 + 0x16;
                                int num119 = num52 + 0x2b;
                                if ( m_vMultiPool == null ) {
                                    m_vMultiPool = VertexConstructor.Create();
                                }
                                for ( int num120 = 0; num120 < num24; num120++ ) {
                                    MultiItem item9 = ( MultiItem )Engine.m_MultiList[ num120 ];
                                    if ( ( ( item9.X == ( x - num21 ) ) && ( item9.Y == ( y - num22 ) ) ) && ( item9.Z == 0 ) ) {
                                        int num121 = item9.ItemID & 0x3fff;
                                        AnimData data2 = Map.GetAnim( num121 );
                                        Texture texture5 = null;
                                        if ( ( data2.frameCount == 0 ) || !Map.m_ItemFlags[ num121 ][ TileFlag.Animation ] ) {
                                            texture5 = hue.GetItem( item9.ItemID );
                                        } else {
                                            texture5 = hue.GetItem( item9.ItemID + data2[ ( m_Frames / ( data2.frameInterval + 1 ) ) % data2.frameCount ] );
                                        }
                                        if ( ( texture5 != null ) && !texture5.IsEmpty() ) {
                                            texture5.DrawGame( num118 - ( texture5.Width >> 1 ), ( num119 - ( ( num23 + item9.Z ) << 2 ) ) - texture5.Height );
                                        }
                                    } else if ( ( ( item9.X + num21 ) > x ) || ( ( ( item9.X + num21 ) == x ) && ( ( item9.Y + num22 ) >= y ) ) ) {
                                        break;
                                    }
                                }
                            }
                            for ( int m = 0; m < list2.Count; m++ ) {
                                DrawQueueEntry entry = ( DrawQueueEntry )list2[ m ];
                                if ( ( entry.m_TileX == x ) && ( entry.m_TileY == y ) ) {
                                    list2.RemoveAt( m );
                                    m--;
                                    entry.m_Texture.Flip = entry.m_Flip;
                                    Clipper clipper = new Clipper( Engine.GameX, num52 - 0x2e, Engine.GameWidth, ( Engine.GameHeight - num52 ) + 0x2e );
                                    PushAlpha( entry.m_fAlpha );
                                    entry.m_Texture.DrawClipped( entry.m_DrawX, entry.m_DrawY, clipper );
                                    PopAlpha();
                                }
                            }
                        }
                        if ( m_Transparency ) {
                            if ( m_vTransDrawPool == null ) {
                                m_vTransDrawPool = VertexConstructor.Create();
                            }
                            while ( transDrawQueue.Count > 0 ) {
                                TransparentDraw draw = ( TransparentDraw )transDrawQueue.Dequeue();
                                PushAlpha( draw.m_fAlpha * 0.5f );
                                draw.m_Texture.DrawGame( draw.m_X, draw.m_Y );
                                if ( draw.m_Double ) {
                                    draw.m_Texture.DrawGame( draw.m_X + 5, draw.m_Y + 5 );
                                }
                                PopAlpha();
                                draw.Dispose();
                            }
                        }
                        SetTexture( null );
                        while ( miniHealthQueue.Count > 0 ) {
                            int num125;
                            int num126;
                            MiniHealthEntry entry2 = ( MiniHealthEntry )miniHealthQueue.Dequeue();
                            Mobile mobile3 = entry2.m_Mobile;
                            TransparentRect( 0, entry2.m_X - 0x10, entry2.m_Y + 8, 0x20, 7 );
                            double num123 = ( ( double )mobile3.CurrentHitPoints ) / ( ( double )mobile3.MaximumHitPoints );
                            if ( num123 == double.NaN ) {
                                num123 = 0.0;
                            } else if ( num123 < 0.0 ) {
                                num123 = 0.0;
                            } else if ( num123 > 1.0 ) {
                                num123 = 1.0;
                            }
                            int width = ( int )( ( 30.0 * num123 ) + 0.5 );
                            MobileFlags flags2 = mobile3.Flags;
                            if ( mobile3.IsPoisoned ) {
                                num125 = 0xff00;
                                num126 = 0x8000;
                            } else if ( flags2[ MobileFlag.YellowHits ] ) {
                                num125 = 0xffc000;
                                num126 = 0x806000;
                            } else {
                                num125 = 0x20c0ff;
                                num126 = 0x106080;
                            }
                            PushAlpha( 0.6f );
                            GradientRect( num125, num126, entry2.m_X - 15, entry2.m_Y + 9, width, 5 );
                            GradientRect( 0x640000, 0xc80000, ( entry2.m_X - 15 ) + width, entry2.m_Y + 9, 30 - width, 5 );
                            PopAlpha();
                            entry2.Dispose();
                        }
                        if ( Engine.m_Ingame ) {
                            Engine.Effects.Draw();
                            if ( ( eOffsetX != 0 ) || ( eOffsetY != 0 ) ) {
                                Engine.Effects.Offset( eOffsetX, eOffsetY );
                            }
                        }
                        Map.Unlock();
                        if ( _profile != null ) {
                            _profile._worldTime.Stop();
                        }
                        SetViewport( 0, 0, Engine.ScreenWidth, Engine.ScreenHeight );
                        if ( lightTexture != null ) {
                            int globalLight = Engine.Effects.GlobalLight;
                            if ( player != null ) {
                                globalLight -= player.LightLevel;
                            }
                            if ( globalLight < 0 ) {
                                globalLight = 0;
                            } else if ( globalLight > 0x1f ) {
                                globalLight = 0x1f;
                            }
                            if ( globalLight != 0 ) {
                                PushAlpha( ( ( float )globalLight ) / 31f );
                                SetBlendType( DrawBlendType.LightSource );
                                lightTexture.Draw( Engine.GameX, Engine.GameY );
                                SetBlendType( DrawBlendType.Normal );
                                PopAlpha();
                            }
                        }
                    }
                    if ( !Engine.m_Ingame ) {
                        Texture sallosDragon = Engine.ImageCache.SallosDragon;
                        if ( sallosDragon != null ) {
                            sallosDragon.Draw( ( Engine.ScreenWidth - sallosDragon.Width ) / 2, ( Engine.ScreenHeight - sallosDragon.Height ) / 2 );
                        }
                    }
                    if ( !Engine.m_Loading ) {
                        MessageManager.BeginRender();
                        if ( Engine.m_Ingame && ( m_TextSurface != null ) ) {
                            SetTexture( null );
                            if ( ( ( player != null ) && player.OpenedStatus ) && ( player.StatusBar == null ) ) {
                                PushAlpha( 0.5f );
                                SolidRect( 0, Engine.GameX + 2, ( Engine.GameY + Engine.GameHeight ) - 0x15, Engine.GameWidth - 0x2e, 0x13 );
                                PopAlpha();
                                int num128 = ( Engine.GameX + Engine.GameWidth ) - 0x2c;
                                int num129 = ( Engine.GameY + Engine.GameHeight ) - 0x15;
                                SolidRect( 0, num128, num129, 0x2a, 0x13 );
                                num128++;
                                num129++;
                                if ( player.Ghost ) {
                                    GradientRect( 0xc0c0c0, 0x606060, num128, num129, 40, 5 );
                                    num129 += 6;
                                    GradientRect( 0xc0c0c0, 0x606060, num128, num129, 40, 5 );
                                    num129 += 6;
                                    GradientRect( 0xc0c0c0, 0x606060, num128, num129, 40, 5 );
                                } else {
                                    int num131;
                                    int num132;
                                    int num130 = ( int )( ( ( ( double )player.CurrentHitPoints ) / ( ( double )player.MaximumHitPoints ) ) * 40.0 );
                                    if ( num130 > 40 ) {
                                        num130 = 40;
                                    } else if ( num130 < 0 ) {
                                        num130 = 0;
                                    }
                                    MobileFlags flags3 = player.Flags;
                                    if ( player.IsPoisoned ) {
                                        num131 = 0xff00;
                                        num132 = 0x8000;
                                    } else if ( flags3[ MobileFlag.YellowHits ] ) {
                                        num131 = 0xffc000;
                                        num132 = 0x806000;
                                    } else {
                                        num131 = 0x20c0ff;
                                        num132 = 0x106080;
                                    }
                                    GradientRect( num131, num132, num128, num129, num130, 5 );
                                    GradientRect( 0xff0000, 0x800000, num128 + num130, num129, 40 - num130, 5 );
                                    num129 += 6;
                                    num130 = ( int )( ( ( ( double )player.CurrentMana ) / ( ( double )player.MaximumMana ) ) * 40.0 );
                                    if ( num130 > 40 ) {
                                        num130 = 40;
                                    } else if ( num130 < 0 ) {
                                        num130 = 0;
                                    }
                                    GradientRect( 0x20c0ff, 0x106080, num128, num129, 40, 5 );
                                    GradientRect( 0xff0000, 0x800000, num128 + num130, num129, 40 - num130, 5 );
                                    num129 += 6;
                                    num130 = ( int )( ( ( ( double )player.CurrentStamina ) / ( ( double )player.MaximumStamina ) ) * 40.0 );
                                    if ( num130 > 40 ) {
                                        num130 = 40;
                                    } else if ( num130 < 0 ) {
                                        num130 = 0;
                                    }
                                    GradientRect( 0x20c0ff, 0x106080, num128, num129, 40, 5 );
                                    GradientRect( 0xff0000, 0x800000, num128 + num130, num129, 40 - num130, 5 );
                                }
                            } else {
                                PushAlpha( 0.5f );
                                SolidRect( 0, Engine.GameX + 2, ( Engine.GameY + Engine.GameHeight ) - 0x15, Engine.GameWidth - 4, 0x13 );
                                PopAlpha();
                            }
                            m_vTextCache.Draw( m_TextSurface, Engine.GameX + 2, ( ( Engine.GameY + Engine.GameHeight ) - 2 ) - m_TextSurface.Height );
                        }
                        if ( _profile != null ) {
                            _profile._gumpTime.Start();
                        }
                        Gumps.Draw();
                        if ( _profile != null ) {
                            _profile._gumpTime.Stop();
                        }
                    }
                    if ( Engine.m_Ingame ) {
                        int gameY = Engine.GameY;
                        int gameHeight = Engine.GameHeight;
                        if ( m_TextSurface != null ) {
                            int height = m_TextSurface.Height;
                        }
                        ArrayList rectsList = m_RectsList;
                        if ( rectsList == null ) {
                            rectsList = m_RectsList = new ArrayList();
                        } else if ( rectsList.Count > 0 ) {
                            rectsList.Clear();
                        }
                        World.DrawAllMessages();
                        textToDrawList.Sort();
                        int num133 = textToDrawList.Count;
                        for ( int num134 = 0; num134 < num133; num134++ ) {
                            TextMessage message = ( TextMessage )textToDrawList[ num134 ];
                            int num135 = message.X + Engine.GameX;
                            int num136 = message.Y + Engine.GameY;
                            if ( num135 < ( Engine.GameX + 2 ) ) {
                                num135 = Engine.GameX + 2;
                            } else if ( ( num135 + message.Image.Width ) >= ( ( Engine.GameX + Engine.GameWidth ) - 2 ) ) {
                                num135 = ( ( Engine.GameX + Engine.GameWidth ) - message.Image.Width ) - 2;
                            }
                            if ( num136 < ( Engine.GameY + 2 ) ) {
                                num136 = Engine.GameY + 2;
                            } else if ( ( num136 + message.Image.Height ) >= ( ( Engine.GameY + Engine.GameHeight ) - 2 ) ) {
                                num136 = ( ( Engine.GameY + Engine.GameHeight ) - message.Image.Height ) - 2;
                            }
                            rectsList.Add( new Rectangle( num135, num136, message.Image.Width, message.Image.Height ) );
                        }
                        for ( int num137 = 0; num137 < num133; num137++ ) {
                            TextMessage message2 = ( TextMessage )textToDrawList[ num137 ];
                            Rectangle rect = ( Rectangle )rectsList[ num137 ];
                            float num138 = 1f;
                            int num139 = rectsList.Count;
                            for ( int num140 = num137 + 1; num140 < num139; num140++ ) {
                                Rectangle rectangle3 = ( Rectangle )rectsList[ num140 ];
                                if ( rectangle3.IntersectsWith( rect ) ) {
                                    num138 += ( ( TextMessage )textToDrawList[ num140 ] ).Alpha;
                                }
                            }
                            num138 = 1f / num138;
                            if ( message2.Disposing ) {
                                num138 *= message2.Alpha;
                            }
                            PushAlpha( num138 );
                            message2.Draw( rect.X, rect.Y );
                            PopAlpha();
                        }
                        if ( ( eOffsetX != 0 ) || ( eOffsetY != 0 ) ) {
                            World.Offset( eOffsetX, eOffsetY );
                        }
                    }
                    if ( !Engine.m_Loading ) {
                        Cursor.Draw();
                    }
                    PushAll();
                } catch ( SharpDXException exception ) {
                    int code = exception.ResultCode.Code;
                    if ( code == ResultCode.DeviceLost.Code ) {
                        try {
                            parametersArray = new PresentParameters[] { Engine.m_PresentParams };
                            Engine.m_Device.Reset( parametersArray );
                            Engine.OnDeviceReset( null, null );
                        } catch {
                        }
                        Application.DoEvents();
                        Thread.Sleep( 10 );
                    } else if ( code == ResultCode.DeviceNotReset.Code ) {
                        parametersArray = new PresentParameters[] { Engine.m_PresentParams };
                        Engine.m_Device.Reset( parametersArray );
                        Engine.OnDeviceReset( null, null );
                        GC.Collect();
                    } else {
                        Thread.Sleep( 10 );
                        try {
                            parametersArray = new PresentParameters[] { Engine.m_PresentParams };
                            Engine.m_Device.Reset( parametersArray );
                            Engine.OnDeviceReset( null, null );
                        } catch {
                        }
                        Debug.Error( exception );
                    }
                } catch ( Exception exception2 ) {
                    Debug.Trace( "Draw Exception:" );
                    Debug.Error( exception2 );
                } finally {
                    Engine.m_Device.EndScene();
                }
                if ( _profile != null ) {
                    _profile._drawTime.Stop();
                }
                try {
                    Engine.m_Device.Present();
                } catch ( SharpDXException exception3 ) {
                    int num142 = exception3.ResultCode.Code;
                    if ( num142 == ResultCode.DeviceLost.Code ) {
                        try {
                            parametersArray = new PresentParameters[] { Engine.m_PresentParams };
                            Engine.m_Device.Reset( parametersArray );
                            Engine.OnDeviceReset( null, null );
                        } catch {
                        }
                        Application.DoEvents();
                        Thread.Sleep( 10 );
                    } else if ( num142 == ResultCode.DeviceNotReset.Code ) {
                        parametersArray = new PresentParameters[] { Engine.m_PresentParams };
                        Engine.m_Device.Reset( parametersArray );
                        Engine.OnDeviceReset( null, null );
                        GC.Collect();
                    } else {
                        Thread.Sleep( 10 );
                        try {
                            parametersArray = new PresentParameters[] { Engine.m_PresentParams };
                            Engine.m_Device.Reset( parametersArray );
                            Engine.OnDeviceReset( null, null );
                        } catch {
                        }
                        Debug.Error( exception3 );
                    }
                }
                m_Count = 0;
                if ( screenshotContext != null ) {
                    screenshotContext.Save();
                    screenshotContext = null;
                }
                m_ActFrames++;
                while ( toUpdateQueue.Count > 0 ) {
                    Mobile mobile4 = ( Mobile )toUpdateQueue.Dequeue();
                    mobile4.MovedTiles++;
                    mobile4.Update();
                }
                Map.Unlock();
            }
        }
    }
}
