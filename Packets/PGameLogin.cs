﻿namespace Injection.Packets
{
    using PlayUO;

    internal class PGameLogin : Packet
    {
        public PGameLogin( int key, string username, string password )
            : base( 0x91, 0x41 ) {
            this.m_Stream.Write( key );
            this.m_Stream.Write( username, 30 );
            this.m_Stream.Write( password, 30 );
        }
    }
}
