﻿namespace Injection.Packets
{
    using PlayUO;

    internal class PSelectServer : Packet
    {
        public PSelectServer( short id )
            : base( 0xa0, 3 ) {
            this.m_Stream.Write( id );
        }
    }
}
