﻿namespace Injection.Packets
{
    using PlayUO;

    internal class GenericCharacter : Packet
    {
        public GenericCharacter(string name)
            : base(0x00, 104)
        {
            this.m_Stream.Write((uint)0xedededed); //W pattern1
            this.m_Stream.Write((uint)0xFFFFFFFF); //W pattern1
            this.m_Stream.Write((int)0); // unknown
            this.m_Stream.Write(name, 30); // char name (0 terminated)
            this.m_Stream.Write((short)0); // unknown
            this.m_Stream.Write((int)0x01); //client flags
            this.m_Stream.Write((int)1); // unknown1
            this.m_Stream.Write((int)1); //  logincount
            this.m_Stream.Write((int)1); // profession
            for(int i=0; i < 15; i++) { // byte[15] 0x00
                this.m_Stream.Write((int)0);
            }
            this.m_Stream.Write((int)0); // gender and race
            this.m_Stream.Write((int)10); // str
            this.m_Stream.Write((int)10); // dex
            this.m_Stream.Write((int)60); // int
            this.m_Stream.Write((int)SkillName.Magery); // skill 1
            this.m_Stream.Write((int)50); // skill 1
            this.m_Stream.Write((int)SkillName.ResistingSpells); // skill 2
            this.m_Stream.Write((int)49); // skill 2
            this.m_Stream.Write((int)SkillName.Alchemy); // skill 3
            this.m_Stream.Write((int)1); // skill 3
            for (int i = 0; i < 24; i++)
            {
                this.m_Stream.Write((int)0); // all 0 index styles
            }
        }
    }
}
