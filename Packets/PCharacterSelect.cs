﻿namespace Injection.Packets
{
    using PlayUO;

    internal class PCharacterSelect : Packet
    {
        public PCharacterSelect( string name, int index, int address )
            : base( 0x5d, 0x49 ) {
            this.m_Stream.Write( ( uint )0xedededed ); //W pattern1
            this.m_Stream.Write( name, 30 ); // char name (0 terminated)
            this.m_Stream.Write( ( short )0 ); // unknown
            this.m_Stream.Write( ( int )0x1f ); // clientflag. We should pick a unique flag for UltimaXNA.
            this.m_Stream.Write( ( int )1 ); // unknown1
            this.m_Stream.Write( ( int )0x18 ); //  logincount
            this.m_Stream.Write( "", 0x10 ); // unknown2
            this.m_Stream.Write( index ); // slot choosen
            this.m_Stream.Write( address ); // clientIP
        }
    }
}
