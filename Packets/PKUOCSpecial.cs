﻿namespace Injection.Packets
{
    using PlayUO;

    internal class PKUOCSpecial : Packet
    {
        public PKUOCSpecial()
            : base( 240, 16 ) {
            base.m_Stream.Write( ( byte )0 );
            base.m_Stream.Write( ( byte )4 );
            base.m_Stream.Write( ( byte )255 );

            Seed.AddTextMessage( "Sending PKUOCSpecial" );
        }
    }

}
