﻿namespace Injection.Packets
{
    using PlayUO;

    internal class PPE_Login : Packet
    {
        public PPE_Login( string username, string password )
            : base( 0x80, 62 ) {
            this.m_Encode = false;
            this.m_Stream.Write( username, 30 );
            this.m_Stream.Write( password, 30 );
            this.m_Stream.Write( ( byte )0x5D );
        }
    }
}
