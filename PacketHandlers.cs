﻿namespace Injection {
    using System;
    using System.Collections.Generic;
    using PlayUO;
    using PlayUO.Profiles;
    using PlayUO.Targeting;
    using Ultima.Data;

    using Game = PlayUO.PacketHandlers;
    using Packets;
    using Injection.Launcher.Controls;
    internal class PacketHandlers {

        static PacketHandlers()
        {
            Game.Register( 0x8c, 11, new PacketCallback( ServerSecret ) );
            Game.Register( 0xa9, -1, new PacketCallback( Characters ) );
            Game.Register( 0xa8, -1, new PacketCallback( SelectServer ) );
            //Game.Register( 0x55, 1, new PacketCallback( LoginComplete ) );
            Game.Register( 240, -1, new PacketCallback( Custom ) );
            // Game.Register(0xBF, -1, new PacketCallback();
            //MacroHandlers.Register( new ActionCallback( Target ), "Cheats|Automation|Target" );
        }

        private static bool Target( string args )
        {
            Engine.AddTextMessage( ( "Located at " + ( World.Player.X + ", " + World.Player.Y ) ) );

            TileFlags flags = Map.GetLandFlags( (int)Map.Felucca.GetLandTile( World.Player.X, World.Player.Y ).landId );
            
            MapBlock mb = Map.Felucca.GetBlock( World.Player.X, World.Player.Y );
            return
                true;
        }

        private static void Characters( PacketReader pvSrc )
        {
            //System.Windows.Forms.MessageBox.Show( "Characters Loading" );

            var characters = new List<CharacterInfo>( pvSrc.ReadByte() );
            for ( int i = 0; i < characters.Capacity; i++ ) {
                string name = pvSrc.ReadString( 30 );
                string password = pvSrc.ReadString( 30 );
                if ( name != "" ) {
                    var info = new CharacterInfo( name, password, i );
                    characters.Add( info );
                }
            }

            int startingLocations = pvSrc.ReadByte();
            for ( int i = 0; i < startingLocations; i++ ) {
                var number = pvSrc.ReadByte();
                var cityName = pvSrc.ReadString( 31 );
                var areaOfCityOrTown = pvSrc.ReadString( 31 );
            }

            CharacterSelect characterSelect = new CharacterSelect();
            characterSelect.Update( characters.ToArray() );
            Injection.Launcher.Shell.MainForm.Panel.Controls.Clear();
            Injection.Launcher.Shell.MainForm.Panel.Controls.Add(characterSelect);
            characterSelect.Controls.Add(new Injection.Launcher.Controls.Display());
            //characterSelect.ShowDialog();
        }
        private static void SelectServer( PacketReader pvSrc )
        {
            //System.Windows.Forms.MessageBox.Show( "Login" );
            pvSrc.ReadByte();
            var shardCount = pvSrc.ReadUInt16();
            for ( int i = 1; i <= shardCount; i++ ) {
                pvSrc.ReadUInt16();
                pvSrc.ReadString( 32 );
                pvSrc.ReadByte();
                pvSrc.ReadByte();
                pvSrc.ReadInt32();
            }

            if ( shardCount < 1 )
                throw new Exception( "No shards" );

            Network.Send( new PSelectServer( 0 ) );
            Network.Flush();
        }
        private static void LoginComplete( PacketReader pvSrc )
        {
            Music.Stop();
            Seed.Unlock();
            Seed.m_Loading = false;
            Seed.m_Ingame = true;
            Cursor.Hourglass = false;
            Seed.ClearScreen();
            Mobile player = World.Player;
            Preferences.Current.Layout.Apply( true );
            Seed.DrawNow();
            Seed.StartPings();
            Network.Send( new POpenPaperdoll() );
            World.Player.QueryStats();
            if ( !ShaderData.IsSupported ) {
                Seed.AddTextMessage( "*** Your video card does not support the 2.0 shader model.", ( float )30f );
                Seed.AddTextMessage( "*** Game graphics will not be rendered correctly.", ( float )30f );
            }
            // GameInjector.Initialize(); // We can enable the use of plugins here, but do we want to override them now?
        }
        private static void ServerSecret( PacketReader pvSrc )
        {
            pvSrc.ReadInt32();
            pvSrc.ReadInt16();

            int shadowKey = pvSrc.ReadInt32();

            Network.Context._crypto = new GameCrypto( 0 );

            //System.Windows.Forms.MessageBox.Show( string.Format( "Relay: {0}", key ) );

            Network.Send( new PGameLogin( shadowKey, Seed.Username, Seed.Password ) );
        }
        private static void Custom( PacketReader pvSrc )
        {
            int position = pvSrc.Position;
            switch ( pvSrc.ReadByte() ) {
                //case 1:
                //Game.Custom_AckPartyLocs( pvSrc );
                //Network.Send( new PPE_QueryPartyLocsEx() );
                //return;
                case 5:
                    // Custom_Extension(pvSrc);
                    /* *
                     * This method contains code that is harmful; another computer could remote into us.
                     * */
                    return;
                case 254:
                    Network.Send( new PKUOCSpecial() );
                    return;
                default:
                    pvSrc.Position = position;
                    PacketHandlers.Custom( pvSrc );
                    return;
            }
        }
    }
}