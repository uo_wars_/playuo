﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;

namespace Injection.Launcher {
    internal sealed class Resolver {

        public static readonly byte[] Buffer = Encoding.ASCII.GetBytes( "2593" );

        private IPEndPoint LocalEP;

        private Ping Ping;
        private PingReply Reply;

        public Resolver( IPEndPoint localEP )
        {
            if ( localEP == null )
                throw new ArgumentNullException( "localEP" );

            this.LocalEP = localEP;
            this.Ping = new Ping();
        }

        internal Ping GetPing()
        {
            return this.Ping;
        }
        internal PingReply GetReply()
        {
            return this.Reply;
        }
        
        public DateTime LastEcho
        {
            get;
            private set;
        }

        public void Echo()
        {
            if ( this.Ping == null )
                this.Ping = new Ping();
            if ( this.Reply == null ) {
                if ( ( this.LastEcho - DateTime.Now ) > TimeSpan.FromSeconds( 3.0 ) )
                    Console.WriteLine( "Resolver Echoing.." );
                this.Reply = this.Ping.Send( this.LocalEP.Address );
            }
        }
    }
}
