﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Injection.Launcher {

    using Controls;
    using PlayUO;
    using Cursor = System.Windows.Forms.Cursor;

    public partial class Shell : Form {
        public static Shell MainForm;
        public Shell()
        {
            InitializeComponent();
        }

        [STAThread]
        static void Main( string[] args )
        {
            
            if ( args.Length > 0 ) {
                Engine.Main( args );
                return;
            }
         
            MainForm = new Shell();
            MainForm.ShowDialog();
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            Cursor.Show();
            base.OnMouseMove(e);
        }

        protected virtual void Shell_Load( object sender, EventArgs e )
        {
            this.Panel.Controls.Add( new Login() );
            foreach ( Control control in this.Panel.Controls ) {
                Console.WriteLine( "Loaded {0} into the shell panel.", control.Name );
            }
        }

        internal void Acquire( Control control )
        {
            this.Acquire( control, true );
        }
        internal void Acquire( Control control, bool refresh )
        {
            if ( control != null ) {
                this.Panel.Controls.Add( control );
                if ( refresh )
                    this.Panel.Refresh();
            }
        }
        internal void Acquire( params Control[] controls )
        {
            if ( controls == null )
                throw new ArgumentNullException( "controls" );

            foreach ( var item in controls ) {
                if ( item == null )
                    continue;

                this.Acquire( item, false );
            }
            this.Panel.Refresh();
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Shell));
            this.Panel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // Panel
            // 
            this.Panel.BackColor = System.Drawing.Color.Transparent;
            this.Panel.Location = new System.Drawing.Point(149, 354);
            this.Panel.Name = "Panel";
            this.Panel.Size = new System.Drawing.Size(500, 150);
            this.Panel.TabIndex = 0;
            this.Panel.TabStop = true;
            this.Panel.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel_Paint);
            // 
            // Shell
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(790, 567);
            this.Controls.Add(this.Panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Shell";
            this.Cursor = Cursors.Arrow;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ultima Online: Kal Vas Flam - Game Launcher - Private Alpha Build";
            this.Load += new System.EventHandler(this.Shell_Load);
            this.ResumeLayout(false);

        }

        internal System.Windows.Forms.Panel Panel;

        private void Panel_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
