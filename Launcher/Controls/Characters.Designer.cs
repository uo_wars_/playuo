﻿namespace Injection.Launcher.Controls {
    partial class Characters {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) ) {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_CharacterBackButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_CharacterBackButton
            // 
            this.m_CharacterBackButton.BackColor = System.Drawing.Color.FromArgb( ( ( int )( ( ( byte )( 47 ) ) ) ), ( ( int )( ( ( byte )( 52 ) ) ) ), ( ( int )( ( ( byte )( 45 ) ) ) ) );
            this.m_CharacterBackButton.Cursor = System.Windows.Forms.Cursors.Default;
            this.m_CharacterBackButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m_CharacterBackButton.Font = new System.Drawing.Font( "Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 0 ) ) );
            this.m_CharacterBackButton.ForeColor = System.Drawing.SystemColors.Desktop;
            this.m_CharacterBackButton.Location = new System.Drawing.Point( 3, 177 );
            this.m_CharacterBackButton.Name = "m_CharacterBackButton";
            this.m_CharacterBackButton.Size = new System.Drawing.Size( 90, 25 );
            this.m_CharacterBackButton.TabIndex = 1;
            this.m_CharacterBackButton.TabStop = false;
            this.m_CharacterBackButton.Text = "Back";
            this.m_CharacterBackButton.UseVisualStyleBackColor = false;
            // 
            // Characters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.m_CharacterBackButton );
            this.Name = "Characters";
            this.Size = new System.Drawing.Size( 566, 205 );
            this.Load += new System.EventHandler( this.Characters_Load );
            this.ResumeLayout( false );

        }

        #endregion

        protected System.Windows.Forms.Button m_CharacterBackButton;
    }
}
