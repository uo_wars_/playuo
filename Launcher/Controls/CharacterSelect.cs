﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System;
using System.Net;
using System.Windows.Forms;

namespace Injection.Launcher.Controls {
    using PlayUO;
    using Packets;
    using Cursor = System.Windows.Forms.Cursor;
    public partial class CharacterSelect : UserControl {
        private Button newCharacterButton;
        private TextBox textBoxName;
        private Label label1;
        private CharacterInfo[] characterInfo;

        public CharacterSelect()
        {
            InitializeComponent();
        }

        public void Update(CharacterInfo[] characters)
        {
            //CharacterInfo[] characters = new CharacterInfo[] { };\
            characterInfo = characters;

            if (characters.Length > 5)
                throw new ArgumentOutOfRangeException("characters", "Character limit must be less than or equal to five.");

            InitializeComponent();

            this.SuspendLayout();
            for (int i = 0; i < characters.Length; i++)
            {
                var button = new CharButton(characters[i].Name, characters[i].Index);

                button.Click += new EventHandler(button_Click);

                button.Top = 5 + (28 * i);
                button.Left = 150;

                this.Controls.Add(button);
            }

            if (characters.Length < 5)
            {
                newCharacterButton.Top = 5 + 28 * characters.Length;
                newCharacterButton.Left = 150;
                newCharacterButton.Visible = true;
            }
            else
            {
                newCharacterButton.Visible = false;
            }

            this.ResumeLayout(true);
        }

        /// <summary>
        /// dis code doe
        /// </summary>
        /// <param name="sender">Captured instance object</param>
        /// <param name="e">Event arguments</param>
        static void button_Click(object sender, EventArgs e)
        {
            var button = (sender as CharButton);

            try
            {
                Shell.MainForm.Hide();
                Network.Send(new PCharacterSelect(button.Text, button.Index, button.IPIndex));
                Seed.ShowWindow(Engine.m_Display.Handle, 5);
            }
            catch { }

            button.Parent.Dispose();
            button.Parent = null;
            button = null;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            Cursor.Current = Cursors.Arrow;
            base.OnMouseMove(e);
        }

        private void newCharacterButton_MouseMove(object sender, MouseEventArgs e)
        {
            Cursor.Current = Cursors.Hand;
            //base.OnMouseMove(e);
        }

        private void CharacterSelectForm_Load(object sender, EventArgs e)
        {
            // this.Cursor = new System.Windows.Forms.Cursor(
        }


        private void newCharacterButton_Click(object sender, EventArgs e)
        {
            //MessageBox(textBoxName.Text);
            if (characterInfo.Length < 5)
            {
                Network.Send(new GenericCharacter("Generic Player"));
                Network.Send(new PCharacterSelect("Generic Player", characterInfo.Length + 1, BitConverter.ToInt32(IPAddress.Parse("5.5.5.5").GetAddressBytes(), 0 )));
                Seed.ShowWindow(Engine.m_Display.Handle, 5);
            }

        }


        private void textBoxName_TextChanged(object sender, EventArgs e)
        {

        }

        #region Designer

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.newCharacterButton = new System.Windows.Forms.Button();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 0;
            // 
            // newCharacterButton
            // 
            this.newCharacterButton.Location = new System.Drawing.Point(320, 77);
            this.newCharacterButton.Name = "newCharacterButton";
            this.newCharacterButton.Size = new System.Drawing.Size(200, 25);
            this.newCharacterButton.TabIndex = 1;
            this.newCharacterButton.Text = "Create New Character";
            this.newCharacterButton.UseVisualStyleBackColor = true;
            this.newCharacterButton.Visible = false;
            this.newCharacterButton.Click += new System.EventHandler(this.newCharacterButton_Click);
            this.newCharacterButton.MouseMove += new System.Windows.Forms.MouseEventHandler(this.newCharacterButton_MouseMove);
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(240, 485);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(291, 21);
            this.textBoxName.TabIndex = 2;
            this.textBoxName.Visible = false;
            this.textBoxName.TextChanged += new System.EventHandler(this.textBoxName_TextChanged);
            // 
            // CharacterSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(43)))), ((int)(((byte)(36)))));
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.newCharacterButton);
            this.Controls.Add(this.label1);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "CharacterSelect";
            this.Size = new System.Drawing.Size(500, 150);
            this.Load += new System.EventHandler(this.CharacterSelectForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #endregion


    }
}