﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Windows.Forms;

namespace Injection.Launcher.Controls {


    public partial class Login : UserControl {

        public delegate void Submitter( Account account );
        public static event Submitter Submit;

        static Login()
        {
            Submit += new Submitter( Login_OnSubmit );
        }

        static void Login_OnSubmit( Account account )
        {
            if ( account == null )
                throw new ArgumentNullException( "account" );

            // var root = account.Root; // UserControl

            account.Display();

            var args = new string[] { 
                account.Name.ToString(),
                account.Password.ToString()
            };

            Seed.Main( args );

            account = null;
        }

        public bool Remembering { get; private set; }
        public Account Account { get; private set; }

        public Login( params Control[] controls )
        {
            this.Account = new Account();
            foreach ( var c in controls )
                this.Controls.Add( c );
            InitializeComponent();
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Username = new System.Windows.Forms.Label();
            this.Password = new System.Windows.Forms.Label();
            this.InputUsername = new System.Windows.Forms.TextBox();
            this.InputPassword = new System.Windows.Forms.MaskedTextBox();
            this.Button = new System.Windows.Forms.Button();
            this.Status = new System.Windows.Forms.Label();
            this.Network = new System.Windows.Forms.Timer(this.components);
            this.RememberAccount = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // Username
            // 
            this.Username.AutoSize = true;
            this.Username.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Username.Location = new System.Drawing.Point(131, 32);
            this.Username.Name = "Username";
            this.Username.Size = new System.Drawing.Size(55, 13);
            this.Username.TabIndex = 0;
            this.Username.Text = "Username";
            // 
            // Password
            // 
            this.Password.AutoSize = true;
            this.Password.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Password.Location = new System.Drawing.Point(131, 57);
            this.Password.Name = "Password";
            this.Password.Size = new System.Drawing.Size(53, 13);
            this.Password.TabIndex = 1;
            this.Password.Text = "Password";
            // 
            // InputUsername
            // 
            this.InputUsername.Location = new System.Drawing.Point(192, 29);
            this.InputUsername.Name = "InputUsername";
            this.InputUsername.Size = new System.Drawing.Size(174, 20);
            this.InputUsername.TabIndex = 4;
            // 
            // InputPassword
            // 
            this.InputPassword.AsciiOnly = true;
            this.InputPassword.BeepOnError = true;
            this.InputPassword.Location = new System.Drawing.Point(192, 54);
            this.InputPassword.Name = "InputPassword";
            this.InputPassword.PasswordChar = '●';
            this.InputPassword.Size = new System.Drawing.Size(174, 20);
            this.InputPassword.TabIndex = 5;
            // 
            // Button
            // 
            this.Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Button.ForeColor = System.Drawing.Color.Black;
            this.Button.Location = new System.Drawing.Point(192, 104);
            this.Button.Name = "Button";
            this.Button.Size = new System.Drawing.Size(174, 23);
            this.Button.TabIndex = 6;
            this.Button.Text = "Login";
            this.Button.UseVisualStyleBackColor = true;
            this.Button.Click += new System.EventHandler(this.Button_Click);
            // 
            // Status
            // 
            this.Status.AutoSize = true;
            this.Status.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status.ForeColor = System.Drawing.Color.DimGray;
            this.Status.Location = new System.Drawing.Point(4, 123);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(126, 19);
            this.Status.TabIndex = 7;
            this.Status.Text = "Private Alpha";
            // 
            // Network
            // 
            this.Network.Enabled = true;
            this.Network.Interval = 25;
            this.Network.Tag = "";
            this.Network.Tick += new System.EventHandler(this.Network_Tick);
            // 
            // RememberAccount
            // 
            this.RememberAccount.AutoSize = true;
            this.RememberAccount.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RememberAccount.Location = new System.Drawing.Point(220, 81);
            this.RememberAccount.Name = "RememberAccount";
            this.RememberAccount.Size = new System.Drawing.Size(119, 17);
            this.RememberAccount.TabIndex = 8;
            this.RememberAccount.Text = "Remember Account";
            this.RememberAccount.UseVisualStyleBackColor = true;
            this.RememberAccount.CheckedChanged += new System.EventHandler(this.RememberAccount_CheckedChanged_1);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(43)))), ((int)(((byte)(36)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Controls.Add(this.RememberAccount);
            this.Controls.Add(this.Status);
            this.Controls.Add(this.Button);
            this.Controls.Add(this.InputPassword);
            this.Controls.Add(this.InputUsername);
            this.Controls.Add(this.Password);
            this.Controls.Add(this.Username);
            this.ForeColor = System.Drawing.Color.White;
            this.Name = "Login";
            this.Size = new System.Drawing.Size(500, 150);
            this.Load += new System.EventHandler(this.Login_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private IContainer components;

        private Label Username;
        private Label Password;
        private TextBox InputUsername;
        private MaskedTextBox InputPassword;
        private Label Status;
        private Timer Network;
        private Button Button;
        private CheckBox RememberAccount;
        private Display Display = new Display();

        private void Login_Load( object sender, EventArgs e )
        {
            this.Controls.Add( Display );
            //this.BackColor = Color.Transparent;
        }
        private void RememberAccount_CheckedChanged( object sender, EventArgs e )
        {

        }
        private void Button_Click( object sender, EventArgs e )
        {
            if ( !IsValid( this.InputUsername.Text ) ) {
                MessageBox.Show( "Invalid Username" );
                this.InputUsername.ResetText();
                return;
            }
            if ( !IsValid( this.InputPassword.Text ) ) {
                MessageBox.Show( "Invalid Password" );
                this.InputPassword.ResetText();
                return;
            }
            this.Account.Set( this.Remembering );
            this.Account.Set( this.InputUsername.Text, this.InputPassword.Text );
            this.Account.RootSet( this );
            Login.Submit( this.Account );
            
        }

        private static bool IsValid( string text )
        {
            if ( string.IsNullOrEmpty( text ) )
                return false;
            foreach ( char c in text ) {
                if ( char.IsDigit( c ) )
                    return true;
                if ( char.IsLetter( c ) )
                    return true;
                return false;
            }
            return true;
        }


        static Resolver Resolver = new Resolver( new IPEndPoint( Display.Website.GetDNS( "login.kalvasflam.co" ), 2593 ) );

        private void Network_Tick( object sender, EventArgs e )
        {
            if ( sender is Timer ) {
            }
        }

        private void RememberAccount_CheckedChanged_1(object sender, EventArgs e)
        {

        }
    }
}