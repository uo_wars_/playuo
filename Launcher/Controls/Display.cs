﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace Injection.Launcher.Controls {
    public partial class Display : UserControl {
        public Display()
        {
            InitializeComponent();
        }

        private void Display_Load( object sender, EventArgs e )
        {
            this.BackColor = Color.Transparent;
        }

        private void Rules_LinkClicked( object sender, LinkLabelLinkClickedEventArgs e )
        {
            Website.Fetch( "/rules" ).Open();
        }
        private void TOS_LinkClicked( object sender, LinkLabelLinkClickedEventArgs e )
        {
            Website.Fetch( "/connect" ).Open();
        }

        public sealed class Website : IDisposable {

            public string Name { get; private set; }
            public string Parameters { get; private set; }

            public Website( string name, params string[] parameters )
            {
                this.Name = name;
                this.Parameters = string.Join( "/", parameters );
                this.Info = new ProcessStartInfo( ( "http://" + this.Name + this.Parameters ) )
                { 
                    // UseShellExecute = false,
                    // CreateNoWindow = true
                };
            }

            private Process Process;
            private ProcessStartInfo Info;

            public void Open()
            {
                if ( this.Process == null ) {
                    Process = Process.Start( this.Info );
                }
            }

            #region IDisposable Members

            public void Dispose()
            {
                if ( this.Process != null ) {
                    this.Process.Close();
                    this.Process = null;
                    this.Info = null;
                }
            }

            #endregion

            internal static Website Fetch( params string[] parameters )
            {
                return new Website( "www.kalvasflam.co", parameters );
            }

            public static IPAddress GetDNS( string hostNameOrAddress )
            {
                return Dns.GetHostAddresses( hostNameOrAddress )[ 0 ];
            }

        }

        #region Designer

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Terms = new System.Windows.Forms.LinkLabel();
            this.Rules = new System.Windows.Forms.LinkLabel();
            this.supportLabel = new System.Windows.Forms.LinkLabel();
            this.communityLabel = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // Terms
            // 
            this.Terms.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Terms.AutoSize = true;
            this.Terms.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.Terms.Location = new System.Drawing.Point(381, 57);
            this.Terms.Name = "Terms";
            this.Terms.Size = new System.Drawing.Size(91, 13);
            this.Terms.TabIndex = 1;
            this.Terms.TabStop = true;
            this.Terms.Text = "Connection Guide";
            this.Terms.VisitedLinkColor = System.Drawing.Color.Olive;
            this.Terms.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.TOS_LinkClicked);
            // 
            // Rules
            // 
            this.Rules.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Rules.AutoSize = true;
            this.Rules.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.Rules.Location = new System.Drawing.Point(381, 72);
            this.Rules.Name = "Rules";
            this.Rules.Size = new System.Drawing.Size(33, 13);
            this.Rules.TabIndex = 2;
            this.Rules.TabStop = true;
            this.Rules.Text = "Rules";
            this.Rules.VisitedLinkColor = System.Drawing.Color.Olive;
            this.Rules.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Rules_LinkClicked);
            // 
            // supportLabel
            // 
            this.supportLabel.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.supportLabel.AutoSize = true;
            this.supportLabel.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.supportLabel.Location = new System.Drawing.Point(381, 86);
            this.supportLabel.Name = "supportLabel";
            this.supportLabel.Size = new System.Drawing.Size(45, 13);
            this.supportLabel.TabIndex = 3;
            this.supportLabel.TabStop = true;
            this.supportLabel.Text = "Support";
            this.supportLabel.VisitedLinkColor = System.Drawing.Color.Olive;
            this.supportLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.supportLabel_LinkClicked);
            // 
            // communityLabel
            // 
            this.communityLabel.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.communityLabel.AutoSize = true;
            this.communityLabel.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.communityLabel.Location = new System.Drawing.Point(381, 41);
            this.communityLabel.Name = "communityLabel";
            this.communityLabel.Size = new System.Drawing.Size(60, 13);
            this.communityLabel.TabIndex = 0;
            this.communityLabel.TabStop = true;
            this.communityLabel.Text = "Community";
            this.communityLabel.VisitedLinkColor = System.Drawing.Color.Olive;
            this.communityLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.communityLabel_LinkClicked_1);
            // 
            // Display
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(43)))), ((int)(((byte)(36)))));
            this.Controls.Add(this.communityLabel);
            this.Controls.Add(this.supportLabel);
            this.Controls.Add(this.Rules);
            this.Controls.Add(this.Terms);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Display";
            this.Size = new System.Drawing.Size(500, 150);
            this.Load += new System.EventHandler(this.Display_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel Terms;
        private LinkLabel supportLabel;
        private LinkLabel communityLabel;
        private System.Windows.Forms.LinkLabel Rules;

        #endregion

        private void supportLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Website.Fetch("/support").Open();
        }

        private void communityLabel_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Website.Fetch("/community").Open();
        }

    }
}