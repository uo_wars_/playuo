﻿using System;
using System.Net;
using System.Windows.Forms;
namespace Injection.Launcher.Controls
{
    using PlayUO;
 
    public class CharacterInfo
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public int Index { get; set; }

        public CharacterInfo() {
        }

        public CharacterInfo( string name, string password, int index ) {
            Name = name;
            Password = password;
            Index = index;
        }

        public Button Create()
        {
            return new CharButton( this.Name, this.Index );
        }

        internal sealed class PCharacterSelect : Packet {
            public PCharacterSelect( string name, int index, int address )
                : base( 0x5d, 0x49 )
            {
                this.m_Stream.Write( ( uint )0xedededed ); //W pattern1
                this.m_Stream.Write( name, 30 ); // char name (0 terminated)
                this.m_Stream.Write( ( short )0 ); // unknown
                this.m_Stream.Write( ( int )0x1f ); // clientflag. We should pick a unique flag for UltimaXNA.
                this.m_Stream.Write( ( int )1 ); // unknown1
                this.m_Stream.Write( ( int )0x18 ); //  logincount
                this.m_Stream.Write( "", 0x10 ); // unknown2
                this.m_Stream.Write( index ); // slot choosen
                this.m_Stream.Write( address ); // clientIP
            }
        }
    }
}
