﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace Injection.Launcher.Controls {
    class CharButton : Button {
        static readonly IPAddress m_AddressIndex = IPAddress.Parse( "5.5.5.5" );
        static readonly int m_IPIndex = BitConverter.ToInt32( m_AddressIndex.GetAddressBytes(), 0 );

        public CharButton( string name, int index )
        {
            this.Text = name;
            this.Index = index;
            this.Height = 25;
            this.Width = 200;
        }

        protected override void OnMouseMove(MouseEventArgs mevent)
        {
            Cursor.Current = Cursors.Hand;
            base.OnMouseMove(mevent);
        }

        public int Index { get; set; }
        public int IPIndex { get { return m_IPIndex; } }
    }

}
