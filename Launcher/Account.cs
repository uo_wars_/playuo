﻿using System;

namespace Injection.Launcher.Controls {
    public sealed class Account {

        public delegate void Submitter( Account account );

        public string Name { get; private set; }
        public string Password { get; private set; }
        public bool RememberInfo { get; private set; }
        public object Root { get; private set; }

        public Account()
        {
            this.Set( string.Empty, string.Empty );
        }
        public Account( string name )
        {
            this.Set( name );
        }
        public Account( string name, string password )
        {
            this.Set( name, password );
        }
        public void Set( string name )
        {
            this.Set( name, string.Empty );
        }
        public void Set( string name, string password )
        {
            this.Name = name;
            this.Password = password;
        }
        public void Set( bool rememberAccountInfo )
        {
            this.RememberInfo = rememberAccountInfo;
        }
        internal void RootSet( object root )
        {
            this.Root = root;
        }

        internal void Display()
        {
            Console.WriteLine( " Displaying Account " );
            Console.WriteLine( "\t - Username : {0}", this.Name );
            Console.WriteLine( "\t - Password : {0}", this.Password );
            Console.WriteLine( "\t - Remember : {0}", this.RememberInfo );
            Console.WriteLine( "\t - FormType : {0}", ( this.Root == null ? "null" : this.Root.GetType().Name ) );
        
        }
    }
}
