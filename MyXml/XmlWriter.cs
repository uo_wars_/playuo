﻿using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Xml;

namespace Injection.MyXml {

    /// <summary>
    /// Author >> Link
    /// <para>Comments >> Small and efficient, it works..</para>
    /// <para>Credits >> okaygo, Krrios, Mark Sturgil</para>
    /// <para>Skype >> linkuog</para>
    /// </summary>
    public class XmlWriter {

        private static class Settings {

            public const Formatting Format = Formatting.Indented;

            public const int Indent = 3;
            public const char IndentChar = ' ';

            public const bool StandAlone = true;

        }

        public delegate void Display( string text );

        internal static Display Screen = new Display( delegate( string text )
        {
            if ( Console.Out != null ) {
                Console.Out.WriteLine( text );
            }
        } );

        private bool m_IsComplete;
        private bool m_IsDocument;
        private string m_Name;
        private byte[] m_Buffer;
        private Stream m_Stream;
        private XmlTextWriter m_Xml;

        protected XmlWriter( bool document ) : this( document, null ) { }
        internal XmlWriter( bool document, string name )
        {
            m_IsComplete = false;
            m_IsDocument = document;
            m_Buffer = new byte[ 0 ];
            m_Stream = new MemoryStream();
            m_Xml = new XmlTextWriter( m_Stream, Encoding.ASCII )
            {
                Formatting = this.Format,
                Indentation = this.Indent,
                IndentChar = this.IndentChar,
            };
            if ( string.IsNullOrEmpty( name ) ) {
                name = this.GetType().Name;
            }
            m_Name = name;
            Set( name, m_IsDocument );
        }

        public virtual Formatting Format
        {
            get { return Settings.Format; }
        }
        public virtual int Indent
        {
            get { return Settings.Indent; }
        }
        public virtual char IndentChar
        {
            get { return Settings.IndentChar; }
        }
        public virtual bool StandAlone
        {
            get { return Settings.StandAlone; }
        }

        /// <summary>
        /// Creates a document if specified and creates an element with the unspecified values.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="document">True; writes document</param>
        private void Set( string name, bool document )
        {
            if ( document )
                m_Xml.WriteStartDocument( this.StandAlone );
            m_Xml.WriteStartElement( name.ToLower() );
        }

        /// <summary>
        /// Creates the element.
        /// </summary>
        /// <param name="name"></param>
        public void Set( string name )
        {
            this.Set( name, false );
        }

        /// <summary>
        /// Creates the contents inside of an element.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetElement( string key, string value )
        {
            m_Xml.WriteElementString( key.ToLower(), value.ToLower() );
        }

        /// <summary>
        /// Changes the properties of the curreent element.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetProperty( string key, string value )
        {
            if ( value != null ) {
                m_Xml.WriteAttributeString( key.ToLower(), value.ToLower() );
            }
        }

        /// <summary>
        /// Completes the element, or document.
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public byte[] EndSet( bool document )
        {
            m_Xml.WriteEndElement();

            if ( document )
                m_Xml.WriteEndDocument();

            m_Xml.Flush();

            m_Buffer = ( m_Stream as MemoryStream ).ToArray();

            if ( document ) {
                m_Stream.Close();
                m_Stream = null;
                m_Xml = null;
                m_IsComplete = true;
            }

            return m_Buffer;
        }

        /// <summary>
        /// Completes the element.
        /// </summary>
        public void EndSet()
        {
            var element = EndSet( false );
        }

        internal void Invoke( Display display )
        {
            if ( m_IsComplete ) {
                if ( display == null )
                    throw new ArgumentNullException( "display" );

                display.Invoke( Encoding.ASCII.GetString( m_Buffer ) );
            }
        }
        internal void Invoke()
        {
            this.Invoke( XmlWriter.Screen );
        }
    }
}