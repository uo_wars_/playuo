﻿using System;

namespace Injection.MyXml {
    public class Profile : XmlProfile {

        public Profile()
        {
            base.Set( "key", "value" );
        }

        [STAThread]
        static void Main( string[] args )
        {
            var p = new Profile();
            p.Set( "key", "value" );
            p.Save();
            p = null;
            Console.ReadKey();
        }

    }
}
