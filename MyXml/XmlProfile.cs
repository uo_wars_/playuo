﻿namespace Injection.MyXml {
    public class XmlProfile {

        private XmlWriter m_Writer;

        public XmlProfile()
        {
            m_Writer = new XmlWriter( true, this.GetType().Name );
        }

        public void Save()
        {
            if ( m_Writer != null ) {
                m_Writer.EndSet( true );
                m_Writer.Invoke();
                m_Writer = null;
            }
        }
        public void Set( string key, string value )
        {
            m_Writer.SetElement( key, value );
        }
        public void SetAttribute( string key, string value )
        {
            m_Writer.SetProperty( key, value );
        }
    }

}
